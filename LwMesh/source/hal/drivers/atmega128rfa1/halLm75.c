/*! \file

\brief iLive2 Temperature sensors implementation

\copyright Copyright (C) 2015 SMIR Team / LIMOS / UBP. All rights reserved.
Republication or redistribution of SMIR Software content is prohibited without the prior written consent of SMIR Team.

\author Kun Mean Hou, Xunxing Diao, Hongling Shi et Christophe de Vaulx

*/

/*- Includes -----------------------------------------------------------------*/

#include "halI2c.h"
#include "halLm75.h"
#include <util/delay.h>

/*- Definitions --------------------------------------------------------------*/

#define LM75_ADDR     0x48

/*- Extern -------------------------------------------------------------------*/

// extern void delayms(unsigned int d);

/*- Implementations ----------------------------------------------------------*/


void lm75_init(void)
{
    //  printf("lm75_init\r\n");

#ifdef GPIO_SIM_I2C
    i2c_init();
    _delay_ms(12);
#else
    halI2C_Init();
#endif
}


short lm75_read_temperature(void)
{

    unsigned char Cmd = 0;
    char Val[2];
    short TempReg;

#ifdef GPIO_SIM_I2C
    _delay_ms(50);
    i2c_read_double(LM75_ADDR, (unsigned char*)&Val[0], (unsigned char*)&Val[1]);
#else
    _delay_ms(50);
    halI2C_write_read_Data(LM75_ADDR, &Cmd, (unsigned char *)Val, 1, 2, 50, 100);
#endif

    TempReg = Val[0] << 8;
    TempReg >>= 7 ;
    TempReg  += Val[1] >> 7;

    return TempReg;
}

