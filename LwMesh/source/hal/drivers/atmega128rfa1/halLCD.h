/*! \file

\brief HAL LCD settings

\copyright Copyright (C) 2015 SMIR Team / LIMOS / UBP. All rights reserved.
Republication or redistribution of SMIR Software content is prohibited without the prior written consent of SMIR Team.

\author Kun Mean Hou, Xunxing Diao, Hongling Shi et Christophe de Vaulx

*/

#ifndef _HAL_LCD_H_
#define _HAL_LCD_H_

#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>

void halLCD_Init();
void halLCD_Print(int nNum);
void halLCD_PrintHex(unsigned char Buf[], int nNum);
void halLCD_PrintLoRaFrame(unsigned char Buf[], int nNum);

#ifdef __cplusplus
}
#endif

#endif  //_HAL_LCD_H_
