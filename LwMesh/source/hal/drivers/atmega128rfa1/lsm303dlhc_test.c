/******************** (C) COPYRIGHT 2009 STMicroelectronics ********************
* File Name          : main.c
* Author             : MSH Application Team
* Author             : andrea labombarda
* Revision           : $Revision: 1.5 $
* Date               : $Date: 2010/08/04 12:19:08 $
* Description        : EKSTM32 main file
* HISTORY:
* Date        | Modification                                | Author
* 16/06/2011  | Initial Revision                            | Fabio Tota

********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*
* THIS SOFTWARE IS SPECIFICALLY DESIGNED FOR EXCLUSIVE USE WITH ST PARTS.
*
*******************************************************************************/


/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include "lsm303dlhc_driver.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t response;

/* Extern variables ----------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

//define for example1,2,3,4
//#define __EXAMPLE1__H
//#define __EXAMPLE2__H
//#define __EXAMPLE3__H

#define USB_SIL_Write(a, b, c)
#define SetEPTxValid(a)
#define EKSTM32_LEDToggle(a)
#define sprintf(a, b)

/*******************************************************************************
* Function Name  : main.
* Description    : Main routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void lsm303dlhc_init(void)
{
  //set ODR_ACCELEROMETER (turn ON device)
 response = SetODR(ODR_25Hz);
 if(response==1){
        sprintf((char*)buffer,"\n\rSET_ODR_OK    \n\r\0");
        USB_SIL_Write(EP1_IN, buffer, 19);
        SetEPTxValid(ENDP1);
  }
 //set ODR_MAGNETOMETER (turn ON device)
 response = SetODR_M(ODR_220Hz_M);
 if(response==1){
        sprintf((char*)buffer,"\n\rSET_ODR_M_OK  \n\r\0");
        USB_SIL_Write(EP1_IN, buffer, 19);
        SetEPTxValid(ENDP1);
  }
 //set PowerMode
 response = SetMode(NORMAL);
 if(response==1){
        sprintf((char*)buffer,"SET_MODE_OK     \n\r\0");
        USB_SIL_Write(EP1_IN, buffer, 19);
        SetEPTxValid(ENDP1);
  }
  //set MagnetometerMode
 response = SetModeMag(CONTINUOUS_MODE);
 if(response==1){
        sprintf((char*)buffer,"SET_MODE_MAG_OK \n\r\0");
        USB_SIL_Write(EP1_IN, buffer, 19);
        SetEPTxValid(ENDP1);
  }
 //set Fullscale
 response = SetFullScale(FULLSCALE_2);
 if(response==1){
        sprintf((char*)buffer,"SET_FULLSCALE_OK\n\r\0");
        USB_SIL_Write(EP1_IN, buffer, 19);
        SetEPTxValid(ENDP1);
  }
  //set Magnetometer Gain
 response = SetGainMag(GAIN_450_M);
 if(response==1){
        sprintf((char*)buffer,"SET_GAIN_MAG_OK \n\r\0");
        USB_SIL_Write(EP1_IN, buffer, 19);
        SetEPTxValid(ENDP1);
  }
 //set axis Enable
 response = SetAxis(X_ENABLE | Y_ENABLE | Z_ENABLE);
 if(response==1){
        sprintf((char*)buffer,"SET_AXIS_OK     \n\r\0");
        USB_SIL_Write(EP1_IN, buffer, 19);
        SetEPTxValid(ENDP1);
  }
}

#if 0
void lsm303dlhc_test(void)
{
  uint8_t buffer[26];
  uint8_t position=0, old_position=0;
  AccAxesRaw_t data;
  MagAxesRaw_t dataM;
  i16_t temperature=0;

/******Example 1******/
#ifdef __EXAMPLE1__H
  while(1){
  //get Acceleration Raw data
  response = GetAccAxesRaw(&data);
  if(response==1){
    EKSTM32_LEDToggle(LED1);
    sprintf((char*)buffer, "X=%6d Y=%6d Z=%6d \r\n", data.AXIS_X, data.AXIS_Y, data.AXIS_Z);
    USB_SIL_Write(EP1_IN, buffer, 29);
    SetEPTxValid(ENDP1);
    old_position = position;
  }
 }
#endif /* __EXAMPLE1__H  */


/******Example 2******/
#ifdef __EXAMPLE2__H
 //set Interrupt Threshold
 response = SetInt1Threshold(20);
 if(response==1){
        sprintf((char*)buffer,"SET_THRESHOLD_OK\n\r\0");
        USB_SIL_Write(EP1_IN, buffer, 19);
        SetEPTxValid(ENDP1);
  }
 //set Interrupt configuration (all enabled)
 response = SetInt1Configuration(INT_ZHIE_ENABLE | INT_ZLIE_ENABLE |
                                INT_YHIE_ENABLE | INT_YLIE_ENABLE |
                                INT_XHIE_ENABLE | INT_XLIE_ENABLE );
 if(response==1){
        sprintf((char*)buffer,"SET_INT_CONF_OK \n\r\0");
        USB_SIL_Write(EP1_IN, buffer, 19);
        SetEPTxValid(ENDP1);
  }
 //set Interrupt Mode
 response = SetIntMode(INT_MODE_6D_POSITION);
 if(response==1){
        sprintf((char*)buffer,"SET_INT_MODE    \n\r\0");
        USB_SIL_Write(EP1_IN, buffer, 19);
        SetEPTxValid(ENDP1);
  }

 while(1){
  //get 6D Position
  response = Get6DPosition(&position);
  if((response==1) && (old_position!=position)){
    EKSTM32_LEDToggle(LED1);
    switch (position){
    case UP_SX:   sprintf((char*)buffer,"\n\rposition = UP_SX  \n\r\0");   break;
    case UP_DX:   sprintf((char*)buffer,"\n\rposition = UP_DX  \n\r\0");   break;
    case DW_SX:   sprintf((char*)buffer,"\n\rposition = DW_SX  \n\r\0");   break;
    case DW_DX:   sprintf((char*)buffer,"\n\rposition = DW_DX  \n\r\0");   break;
    case TOP:     sprintf((char*)buffer,"\n\rposition = TOP    \n\r\0");   break;
    case BOTTOM:  sprintf((char*)buffer,"\n\rposition = BOTTOM \n\r\0");   break;
    default:      sprintf((char*)buffer,"\n\rposition = unknown\n\r\0");   break;
    }
  USB_SIL_Write(EP1_IN, buffer, 23);
  SetEPTxValid(ENDP1);
  old_position = position;
  }
 }
#endif /*__EXAMPLE2__H */


/******Example 3******/
#ifdef __EXAMPLE3__H
  while(1){
  //get Magnetometer Raw data
  response = GetMagAxesRaw(&dataM);
  if(response==1){
    EKSTM32_LEDToggle(LED1);
    sprintf((char*)buffer, "xM=%5d yM=%5d zM=%5d \r\n", dataM.AXIS_X, dataM.AXIS_Y, dataM.AXIS_Z);
    USB_SIL_Write(EP1_IN, buffer, 29);
    SetEPTxValid(ENDP1);
    old_position = position;
  }
 }
#endif /* __EXAMPLE3__H  */

 /******Example 4******/
#ifdef __EXAMPLE4__H
 //set temperature
 response = SetTemperature(MEMS_ENABLE);
 if(response==1){
        sprintf((char*)buffer,"SET_TEMP_OK     \n\r\0");
        USB_SIL_Write(EP1_IN, buffer, 19);
        SetEPTxValid(ENDP1);
  }
  while(1){
  //get temperature data value (DegC)
  response = GetTempRaw(&temperature);
  if(response==1){
    EKSTM32_LEDToggle(LED1);
    sprintf((char*)buffer, "Temperature= %d \r\n", temperature);
    USB_SIL_Write(EP1_IN, buffer, 18);
    SetEPTxValid(ENDP1);
    old_position = position;
  }
 }
#endif /* __EXAMPLE4__H  */

} // end main
#endif // 0

#ifdef USE_FULL_ASSERT
/*******************************************************************************
* Function Name  : assert_failed
* Description    : Reports the name of the source file and the source line number
*                  where the assert_param error has occurred.
* Input          : - file: pointer to the source file name
*                  - line: assert_param error line source number
* Output         : None
* Return         : None
*******************************************************************************/
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {}
}
#endif

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
