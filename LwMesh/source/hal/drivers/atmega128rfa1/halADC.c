/*! \file

\brief iLive2 Light sensor and battery module implementation

\copyright Copyright (C) 2015 SMIR Team / LIMOS / UBP. All rights reserved.
Republication or redistribution of SMIR Software content is prohibited without the prior written consent of SMIR Team.

\author Kun Mean Hou, Xunxing Diao, Hongling Shi et Christophe de Vaulx

*/

// Ch0 -- Battery
/*
def battery(ItemVal):
    temp = ord(ItemVal[1]) * 256 + ord(ItemVal[0])
    temp = (temp * 1600 / 1024 * 11 + 360);
    return str(temp)
*/

// Ch1 -- Light
/*
def light_TEPT4400(ItemVal):
    temp = ord(ItemVal[1]) * 256 + ord(ItemVal[0])
    temp = (temp * 1600 / 1024 / 2 / 2);
    return str(temp)
*/

/*
    adc_init();
    appMessage.data.scm_sensors.battery     = adc_read(0);
    appMessage.data.scm_sensors.light       = adc_read(1);
    adc_close();
*/

/*- Includes -----------------------------------------------------------------*/

#include <avr/io.h>
#include <util/delay.h>

#include "halGpio.h"

/*- Definitions --------------------------------------------------------------*/

#define DELAY_FOR_STABILIZE       125

//Micro_iLive
// HAL_GPIO_PIN(D4, D, 4)
//Edu board
HAL_GPIO_PIN(B7, B, 7)

/*- Implementations ----------------------------------------------------------*/

void sensor_power_on(void)
{
    // HAL_GPIO_D4_out();    //Micro_iLive
    // HAL_GPIO_D4_set();

    HAL_GPIO_B7_out();    //Edu board
    HAL_GPIO_B7_set();

    // HAL_GPIO_D5_out();    //Ext_MiLive
    // HAL_GPIO_D5_set();
}

void sensor_power_off(void)
{
    // HAL_GPIO_D5_out();     //Ext_MiLive
    // HAL_GPIO_D5_clr();

    // HAL_GPIO_D4_out();     //Micro_iLive
    // HAL_GPIO_D4_clr();

    HAL_GPIO_B7_out();     //Edu board
    HAL_GPIO_B7_clr();
}

void adc_init(void)
{
    // sensor_power_on();
    _delay_ms(50);

    // AREF = Internal 1.6V Voltage Reference
    ADMUX = (3 << 6);  // Internal 1.6V Voltage Reference

    // ADC Enable and prescaler of 128
    // 16000000/128 = 125000
    ADCSRA = (1 << ADEN) | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
}

uint16_t adc_read(uint8_t ch)
{
    // select the corresponding channel 0~7
    // ANDing with ��7�� will always keep the value
    // of ��ch�� between 0 and 7
    ch &= 0b00000111;  // AND operation with 7

    DIDR0 = (1 << ch);

    ADMUX = (ADMUX & 0xF8) | ch; // clears the bottom 3 bits before ORing

    _delay_us(DELAY_FOR_STABILIZE);

    // start single convertion
    // write ��1�� to ADSC
    ADCSRA |= (1 << ADSC);

    // wait for conversion to complete
    // ADSC becomes ��0�� again
    // till then, run loop continuously
    while (ADCSRA & (1 << ADSC));

    return (ADC);
}

void adc_close(void)
{
    ADMUX  = 0;
    ADCSRA = 0;
    // Digital input enable
    DIDR0 = 0;

    // sensor_power_off();
}
