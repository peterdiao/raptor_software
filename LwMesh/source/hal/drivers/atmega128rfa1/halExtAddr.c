/**************************************************************************//**
  \file  halExtAddr.c (previously, DS2411.c)

 ******************************************************************************/

/******************************************************************************
                   Includes section
******************************************************************************/
#include <halExtAddr.h>
#include "halGpio.h"

/******************************************************************************
                   Defines section
******************************************************************************/
HAL_GPIO_PIN(OWR,  G, 5);


/******************************************************************************
                   Constants section
******************************************************************************/


/******************************************************************************
                   External variables section
******************************************************************************/


/******************************************************************************
                   Implementations section
******************************************************************************/
/**************************************************************************//**
\brief
******************************************************************************/

/* provide a delay of 1us */
#define DELAY_1US {asm volatile("nop");asm volatile("nop");asm volatile("nop");asm volatile("nop");asm volatile("nop");asm volatile("nop");asm volatile("nop");asm volatile("nop");}

/* provide a delay in us range min 4us for delay lower than 4us use the DELAY_1US several time    */
void
micro_delay(unsigned int i)
{
    if (i > 3)
    {
        i -= 3;
    }
    for (; i > 0; i--)
    {
        asm volatile("nop");
        asm volatile("nop");
    }
}

/*----------------------------------------------------------------------------*
                   Implementations section
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*
 reset the 1 wire.
Parameters:
Returns:
  1 if presence detect.
 *----------------------------------------------------------------------------*/
int owr_reset(void)
{
    int result;
    cli();
    HAL_GPIO_OWR_out();
    HAL_GPIO_OWR_clr();
    micro_delay (tRSTL);
    HAL_GPIO_OWR_in();
    micro_delay(tMSP);
    result = HAL_GPIO_OWR_read();
    micro_delay (tRSTL);
    sei ();
    return !result;
}

/*----------------------------------------------------------------------------*
read one byte on 1 wire
Parameters:
Returns:
  value.
 *----------------------------------------------------------------------------*/
unsigned owr_readb(void)
{
    unsigned result = 0;
    int i;
    for (i = 0; i < 8; i++)
    {
        result >>= 1;
        HAL_GPIO_OWR_out();
        micro_delay (tRL);
        HAL_GPIO_OWR_in();
        micro_delay(tMSR);
        if (HAL_GPIO_OWR_read())
        {
            result |= 0x80;
        }
        micro_delay(tSLOT - tRL);
    }
    return result;
}

/*----------------------------------------------------------------------------*
write one byte on 1 wire.
Parameters:
   bytes to write.
Returns:
 *----------------------------------------------------------------------------*/
void owr_writeb(unsigned byte)
{
    int i;
    for (i = 0; i < 8; i++)
    {
        if (byte & 0x01)
        {
            HAL_GPIO_OWR_out();
            micro_delay (tW1L);
            HAL_GPIO_OWR_in();
            micro_delay(tSLOT - tW1L);
        }
        else
        {
            HAL_GPIO_OWR_out();
            micro_delay (tW0L);
            HAL_GPIO_OWR_in();
            micro_delay(tSLOT - tW0L);
        }
        byte >>= 1;
    }
}

/*----------------------------------------------------------------------------*
CRC calculation for ds2411   Polynomial ^8 + ^5 + ^4 + 1
Parameters:
  actual crc and new byte
Returns:
  crc update
 *----------------------------------------------------------------------------*/
static unsigned crc8_add(unsigned a_crc, unsigned byte)
{
    int i;
    a_crc ^= byte;
    for (i = 0; i < 8; i++)
    {
        if (a_crc & 1)
        {
            a_crc = (a_crc >> 1) ^ 0x8c;
        }
        else
        {
            a_crc >>= 1;
        }
    }
    return a_crc;
}

/*----------------------------------------------------------------------------*
ds2411_read_temp.  read the serail on ds2411
Parameters:
  8 bytes char array pointer
Returns:
  0 if error or 1 if ok
  and value in ds2411_id
 *----------------------------------------------------------------------------*/
int ds2411_read(unsigned char *ds2411_id)
{
    int retry;
    int i;
    unsigned family, crc, read_crc;
    retry = 4;
    do
    {
        crc = 0;
        if (owr_reset())   /* Reset ds2411  */
        {
            owr_writeb(0x33);  /* Read ds2411 command */
            family = owr_readb();
            crc = crc8_add (crc, family);
            if (family == 1)
            {
                ds2411_id[7] = 0;
                ds2411_id[6] = 1;
                for (i = 0; i < 6; i++)
                {
                    ds2411_id[i] = owr_readb();
                    crc = crc8_add (crc, ds2411_id[i]);
                }
            }
            else
            {
                crc = 99; /* retry */
            }
        }
        else
        {
            crc = 99; /* retry */
        }
        retry--;
        read_crc = owr_readb();
    }
    while ((crc != read_crc) & (retry > 0));
    if (retry)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/*---------------------------------------------------------------------------*/

// eof DS2411.c
