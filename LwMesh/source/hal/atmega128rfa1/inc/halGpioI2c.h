/*! \file

\brief I2C settings

\copyright Copyright (C) 2015 SMIR Team / LIMOS / UBP. All rights reserved.
Republication or redistribution of SMIR Software content is prohibited without the prior written consent of SMIR Team.

\author Kun Mean Hou, Xunxing Diao, Hongling Shi et Christophe de Vaulx

*/

#ifndef _HAL_GPIO_I2C_H_
#define _HAL_GPIO_I2C_H_

#include "halGpio.h"

#define GPIO_SIM_I2C

HAL_GPIO_PIN(I2C_CLK, D, 0);
HAL_GPIO_PIN(I2C_DATA, D, 1);


/*
#define I2C_EN      HAL_GPIO_B6_out();\
                    HAL_GPIO_B6_set();

#define I2C_DIS     HAL_GPIO_B6_out();\
                    HAL_GPIO_B6_clr();
*/

#define Set_SDA     HAL_GPIO_I2C_DATA_out();\
                    HAL_GPIO_I2C_DATA_set()
#define Clr_SDA     HAL_GPIO_I2C_DATA_out();\
                    HAL_GPIO_I2C_DATA_clr()

#define Set_SCL     HAL_GPIO_I2C_CLK_out();\
                    HAL_GPIO_I2C_CLK_set()
#define Clr_SCL     HAL_GPIO_I2C_CLK_out();\
                    HAL_GPIO_I2C_CLK_clr()

#define Switch_SDA_Input  HAL_GPIO_I2C_DATA_in()

#define Switch_SCL_Input  HAL_GPIO_I2C_CLK_in()

#define Get_SDA  HAL_GPIO_I2C_DATA_read()


void i2c_init(void);

void i2c_deinit(void);

void i2c_write_single(unsigned char ADDR, unsigned char CMD);

void i2c_read_single(unsigned char ADDR, unsigned char *pDATA);

void i2c_read_double(unsigned char ADDR, unsigned char *pDATA1, unsigned char *pDATA2);

void _i2c_delay(void);
void i2c_start(void);
void i2c_stop(void);
unsigned char i2c_tx(unsigned char data);
unsigned char i2c_rx(char ack);

void ADC_PowerOFF(void);
void ADC_PowerON(void);

void i2c_read_reg(unsigned char ADDR, unsigned char REG, unsigned char *pDATA);
void i2c_write_reg(unsigned char ADDR, unsigned char REG, unsigned char CMD);

char i2c_write_string(unsigned char ADDR, unsigned char *reg_data, unsigned char cnt);
char i2c_write_read_string(unsigned char ADDR, unsigned char *in_data, unsigned char *out_data, unsigned char in_cnt, unsigned char out_cnt);

void delayms(unsigned int d);

#endif //_HAL_GPIO_I2C_H_

