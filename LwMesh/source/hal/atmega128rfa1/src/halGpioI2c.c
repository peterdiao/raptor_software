/*! \file

\brief I2C module implementation

\copyright Copyright (C) 2015 SMIR Team / LIMOS / UBP. All rights reserved.
Republication or redistribution of SMIR Software content is prohibited without the prior written consent of SMIR Team.

\author Kun Mean Hou, Xunxing Diao, Hongling Shi et Christophe de Vaulx

*/

/*
#if 0   //For AT91
#define PIN_SDA_OUT_1    {1 << 3, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_1, PIO_DEFAULT}
#define PIN_SDA_OUT_0    {1 << 3, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}
#define PIN_SCL_OUT_1    {1 << 4, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_1, PIO_DEFAULT}
#define PIN_SCL_OUT_0    {1 << 4, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}
#define PIN_SDA_IN       {1 << 3, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_INPUT, PIO_DEFAULT}

static const Pin pin_I2C_SDA_1[] = {PIN_SDA_OUT_1};
static const Pin pin_I2C_SDA_0[] = {PIN_SDA_OUT_0};
static const Pin pin_I2C_SCL_1[] = {PIN_SCL_OUT_1};
static const Pin pin_I2C_SCL_0[] = {PIN_SCL_OUT_0};
static const Pin pin_I2C_SDA_In[] = {PIN_SDA_IN};

#define Set_SDA     PIO_Configure(pin_I2C_SDA_1, PIO_LISTSIZE(pin_I2C_SDA_1));\
                    PIO_Set(pin_I2C_SDA_1)
#define Clr_SDA     PIO_Configure(pin_I2C_SDA_0, PIO_LISTSIZE(pin_I2C_SDA_0));\
                    PIO_Clear(pin_I2C_SDA_0)

#define Set_SCL     PIO_Configure(pin_I2C_SCL_1, PIO_LISTSIZE(pin_I2C_SCL_1));\
                    PIO_Set(pin_I2C_SCL_1)
#define Clr_SCL     PIO_Configure(pin_I2C_SCL_0, PIO_LISTSIZE(pin_I2C_SCL_0));\
                    PIO_Clear(pin_I2C_SCL_0)

#define Switch_SDA_Input  PIO_Configure(pin_I2C_SDA_In, PIO_LISTSIZE(pin_I2C_SDA_In))
#define Get_SDA  PIO_Get(pin_I2C_SDA_In)
#endif

*/


/*- Includes -----------------------------------------------------------------*/

#include "halGpioI2c.h"
//#include "halHsdtvi.h"
#include "stddef.h"

/*- Implementations ----------------------------------------------------------*/

#if 0
void i2c_init(void)
{
    //    PIO_Configure(pin_I2C_SDA_1, PIO_LISTSIZE(pin_I2C_SDA_1));
    //    PIO_Configure(pin_I2C_SCL_1, PIO_LISTSIZE(pin_I2C_SCL_1));

    //  I2C_EN;

    Set_SDA;
    Set_SCL;
}

void i2c_deinit(void)
{
    //  I2C_DIS;

    //  Clr_SDA;
    //  Clr_SCL;
    Set_SCL;
    Set_SDA;

    //  Switch_SDA_Input;
    //  Switch_SCL_Input;
}
#endif // 0

void _i2c_delay(void)
{
    volatile unsigned char d = 0;
    d++; d++; d++;
}

void i2c_start(void)
{
    /* release two wires */
    Set_SDA;
    Set_SCL;
    _i2c_delay();

    /* pull down the SDA */
    Clr_SDA;
    _i2c_delay();

    /* end */
    Clr_SCL;
    _i2c_delay();
}

void i2c_stop(void)
{
    /*  */
    Clr_SDA;
    Set_SCL;
    _i2c_delay();

    /* pull up the SDA */
    Set_SDA;
    _i2c_delay();
}

unsigned char i2c_tx(unsigned char data)
{
    unsigned char i;
    volatile unsigned char d = 0;

    // printf("sending(0x%x) ", data);
    for (i = 0; i < 8; i++)
    {
        /* put data on SDA */
        if (data & 0x80)
        {
            // printf("1 ");
            Set_SDA;
        }
        else
        {
            // printf("0 ");
            Clr_SDA;
        }
        _i2c_delay();

        /* emit the SDA via trigger a square-wave on SCL */
        Set_SCL;
        d++; d++; d++;
        data <<= 1;
        Clr_SCL;
    }
    // printf("\n");
    d++; d++; d++;

    //  Set_SDA;
    Switch_SDA_Input;
    Set_SCL;
    _i2c_delay();
    //  i = IORD_ALTERA_AVALON_PIO_DATA(SDA_BASE); /* ACK bit */
    i = Get_SDA;
    Clr_SCL;

    //  printf("i2c_tx Ack = %d\r\n", i);
    return i;
}

unsigned char i2c_rx(char ack)
{
    char i, data = 0;

    Switch_SDA_Input;
    //  Set_SDA;
    // printf("received: ");
    for (i = 0; i < 8; i++)
    {
        data <<= 1;

        //      do {
        Set_SCL;
        //      } while (IORD_ALTERA_AVALON_PIO_DATA(SCL_BASE) == 0);

        _i2c_delay();
        //      if (IORD_ALTERA_AVALON_PIO_DATA(SDA_BASE))
        if (Get_SDA)
        {
            // printf("1 ");
            data |= 1;
        }
        else
        {
            // printf("0 ");
        }
        Clr_SCL;

        _i2c_delay();
    }
    // printf("\n");

    if (ack)
    {
        Clr_SDA;
    }
    else
    {
        Set_SDA;
    }

    Set_SCL;
    _i2c_delay();         /* sending (N)ACK */
    Clr_SCL;

    //  Set_SDA;
    return data;
}

void i2c_write_single(unsigned char ADDR, unsigned char CMD)
{
    i2c_start();
    i2c_tx(ADDR << 1);
    i2c_tx(CMD);
    i2c_stop();
}

void i2c_read_single(unsigned char ADDR, unsigned char *pDATA)
{
    i2c_start();
    i2c_tx( (ADDR << 1) | 0x01 );
    *pDATA = i2c_rx(0);
    i2c_stop();
}

void i2c_read_double(unsigned char ADDR, unsigned char *pDATA1, unsigned char *pDATA2)
{
    i2c_start();
    i2c_tx( (ADDR << 1) | 0x01 );
    *pDATA1 = i2c_rx(0);
    *pDATA2 = i2c_rx(0);
    i2c_stop();
}


void i2c_read_reg(unsigned char ADDR, unsigned char REG, unsigned char *pDATA)
{
    i2c_start();
    i2c_tx(ADDR << 1);
    i2c_tx(REG);
    i2c_start();
    i2c_tx( (ADDR << 1) | 0x01 );
    *pDATA = i2c_rx(0);
    i2c_stop();
}

void i2c_write_reg(unsigned char ADDR, unsigned char REG, unsigned char CMD)
{
    i2c_start();
    i2c_tx(ADDR << 1);
    i2c_tx(REG);
    i2c_tx(CMD);
    i2c_stop();
}


char i2c_write_string(unsigned char ADDR, unsigned char *reg_data, unsigned char cnt)
{
    unsigned char i;
    if (reg_data == NULL)
    {
        return -1;
    }

    i2c_start();
    i2c_tx(ADDR << 1);
    for (i = 0; i < cnt; i++)
    {
        i2c_tx(reg_data[i]);
    }
    i2c_stop();
    return 0;
}


char i2c_write_read_string(unsigned char ADDR, unsigned char *in_data, unsigned char *out_data, unsigned char in_cnt, unsigned char out_cnt)
{
    unsigned char i;
    if (in_data == NULL)
    {
        return -1;
    }
    if (out_data == NULL)
    {
        return -1;
    }
    if (in_cnt == 0)
    {
        return -1;
    }
    if (out_cnt == 0)
    {
        return -1;
    }

    i2c_start();
    i2c_tx(ADDR << 1);
    for (i = 0; i < in_cnt; i++)
    {
        i2c_tx(in_data[i]);
    }
    i2c_start();
    i2c_tx( (ADDR << 1) | 0x01 );
    if (out_cnt >= 2)
    {
        for (i = 0; i < out_cnt - 1; i++)
        {
            out_data[i] = i2c_rx(1);
        }
    }
    out_data[out_cnt - 1] = i2c_rx(0);
    i2c_stop();
    return 0;
}

#if 0

#define DELAY_CNT 0x200

void delayms(unsigned int d)
{
    volatile unsigned int i, j;
    for (j = 0; j < DELAY_CNT; j++)
    {
        for (i = 0; i < d; i++)
        {
            ;
        }
    }
}

#endif
