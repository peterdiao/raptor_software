/**
 * \file WSNDemo.c
 *
 * \brief WSNDemo application implementation
 *
 * Copyright (C) 2012-2014, Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 * Modification and other use of this code is subject to Atmel's Limited
 * License Agreement (license.txt).
 *
 * $Id: WSNDemo.c 9267 2014-03-18 21:46:19Z ataradov $
 *
 */


#ifdef __cplusplus
extern "C" {
#endif

/*- Includes ---------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "config.h"
#include "hal.h"
#include "phy.h"
#include "sys.h"
#include "nwk.h"
#include "nwkRoute.h"
#include "nwkSecurity.h"
#include "sysTimer.h"
#include "halUart.h"
#include "halUart0.h"
#include "halSleep.h"
#include "halBoard.h"
#include "halLed.h"
#include "halExtAddr.h"
#include "halADC.h"
#include "halLm75.h"
#include "bmp180.h"
#include "commands.h"
#include <util/delay.h>

#ifdef NEO_Pixel
#include "Adafruit_NeoPixel.h"
#endif // NEO_Pixel

#include "halI2c.h"
#include "twi.h"
#include "bmp180.h"
s32 bmp180_read_pressure(void);
s16 bmp180_read_template(void);
s32 bmp180_extern_init(void);
u16 bmp180_get_uncomp_temperature(void);
u32 bmp180_get_uncomp_pressure(void);

#include "lsm303dlhc_test.h"
#include "lsm303dlhc_driver.h"
#include "LSM330DLC.h"
#include "sht1x.h"

#ifdef _SENSOR_DS18B20_
#include "ds18b20.h"

HAL_GPIO_PIN(OneWirePWR,  D, 6);
HAL_GPIO_PIN(OneWireD,  D, 7);
#endif // _SENSOR_DS18B20_

#ifdef _SENSOR_DHT22_
#include "DHT.h"
#endif // _SENSOR_DHT22_

#ifdef RGB_LCD
#include "halLCD.h"
#endif // RGB_LCD

uint8_t g_nSensPacketCnt[3]; // Node Coor, Route, End Sensing Packet Count

#ifdef _SENSOR_DHT22_
#define DHTPIN 6     // what pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302)

DHT dht(DHTPIN, DHTTYPE);
#endif // _SENSOR_DHT22_

#ifdef LORA_433MHZ
void App_SX1278_Test(void);
void App_SX1278_DumpReg(void);
void App_SX1278_Init(void);
void App_SX1278_DeInit(void);
void App_SX1278_TaskHandle(void);
extern unsigned char   PKTSNRVALUE;
extern unsigned char   PKTRSSIVALUE;
extern unsigned char   RSSIVALUE;
extern uint8_t   SX1278_Reg[128];

static SYS_Timer_t appLoRaSendTimer;

#endif //LORA_433MHZ

#ifdef _RS485_SENSOR_
uint16_t CRC16 (const uint8_t *nData, uint16_t wLength);
static void appUart0SendMessage(const uint8_t *data, uint8_t size);
void (*g_RS485Sensor_Handler)(uint16_t buflen);
void appPM25SensorRead(uint16_t bytes);
void appSoilTriInfoSensorRead(uint16_t bytes);
void appSoilPHSensorRead(uint16_t bytes);
void appLightSensorRead(uint16_t bytes);
void appTempHumSensorRead(uint16_t bytes);
void appCO2SensorRead(uint16_t bytes);
static SYS_Timer_t appSensingTimer;
void appSensingInitRS485Com(void);
void appSensingSendCmd(void);
void appSensingPollNextSensor(void);

#define RS485_SENSOR_CMD_MAX_SIZE 10
#define RS485_SENSOR_NAME_MAX_SIZE 16

typedef enum RS485_Sensor_Type_t
{
    ID_RS485_Soil_PH,
    ID_RS485_PM25,
    ID_RS485_CO2,
    ID_RS485_TempHum,
    ID_RS485_Light,
    ID_RS485_SoilTriInfo,
} RS485_Sensor_Type;

typedef struct PACK
{
    RS485_Sensor_Type    SensorID;
    uint8_t    CmdLen;
    uint8_t    Cmd[RS485_SENSOR_CMD_MAX_SIZE];
    char       SensorName[RS485_SENSOR_NAME_MAX_SIZE];
    void (*Sensor_Response_Handler)(uint16_t buflen);
} RS485_Sensor_Com_t;

uint8_t g_CurRS485SensorIndex = 0;

const RS485_Sensor_Com_t g_RS485_Sensor[] = {
    { ID_RS485_Soil_PH,     6,  {0x05, 0x03, 0x00, 0x00, 0x00, 0x01},       "SoilPH",     appSoilPHSensorRead },
    { ID_RS485_PM25,        6,  {0x01, 0x03, 0x00, 0x00, 0x00, 0x01},       "PM25",       appPM25SensorRead },
    { ID_RS485_CO2,         6,  {0x02, 0x03, 0x00, 0x00, 0x00, 0x01},       "CO2",        appCO2SensorRead },
    { ID_RS485_TempHum,     6,  {0x03, 0x03, 0x00, 0x00, 0x00, 0x02},       "TempHum",    appTempHumSensorRead },
    { ID_RS485_Light,       6,  {0x04, 0x03, 0x00, 0x00, 0x00, 0x02},       "Light",      appLightSensorRead },
    { ID_RS485_SoilTriInfo, 7,  {0x06, 0x01, 0x04, 0x00, 0x00, 0x00, 0x03}, "SoilTriInfo", appSoilTriInfoSensorRead },
};
const uint8_t g_RS485_Sensor_Cnt = sizeof(g_RS485_Sensor)/sizeof(RS485_Sensor_Com_t);

#define RELAY_LOW_EN

#ifdef RELAY_LOW_EN
    #define RELAY_ON \
        HAL_GPIO_ARD_IO4_out();\
        HAL_GPIO_ARD_IO4_clr()

    #define RELAY_OFF \
        HAL_GPIO_ARD_IO4_out();\
        HAL_GPIO_ARD_IO4_set()
#else //RELAY_LOW_EN
    #define RELAY_OFF \
        HAL_GPIO_ARD_IO4_out();\
        HAL_GPIO_ARD_IO4_clr()

    #define RELAY_ON \
        HAL_GPIO_ARD_IO4_out();\
        HAL_GPIO_ARD_IO4_set()
#endif // RELAY_LOW_EN

#endif //_RS485_SENSOR_

#ifdef _SHARP_PM25_SENSOR_
    #define SHARP_Sensor_OFF \
        HAL_GPIO_ARD_IO6_out(); \
        HAL_GPIO_ARD_IO6_set();

    #define SHARP_Sensor_ON \
        HAL_GPIO_ARD_IO6_out(); \
        HAL_GPIO_ARD_IO6_clr();

    #define SHARP_LED_OFF \
        HAL_GPIO_ARD_IO7_out(); \
        HAL_GPIO_ARD_IO7_set();

    #define SHARP_LED_ON \
        HAL_GPIO_ARD_IO7_out(); \
        HAL_GPIO_ARD_IO7_clr();
#endif // _SHARP_PM25_SENSOR_

/*- Definitions ------------------------------------------------------------*/
#if defined(APP_COORDINATOR)
#define APP_NODE_TYPE     0
#elif defined(APP_ROUTER)
#define APP_NODE_TYPE     1
#else
#define APP_NODE_TYPE     2
#endif

#define APP_CAPTION_SIZE    (sizeof(APP_CAPTION) - 1)
#define APP_COMMAND_PENDING 0x01

#define APP_ENDPOINT        1

// #define APP_LED_NETWORK     0
// #define APP_LED_DATA        1
/**
Arduino Name	AVRRF Pin
AD0	            PF2/ADC2
AD1	            PF3/ADC3
AD2	            PF4/ADC4/TCK
AD3	            PF5/ADC5/TMS
AD4	            PF6/ADC6/TDO
AD5	            PF7/ADC7/TDI
IO0	            PE0
IO1	            PE1
IO2	            PE2
IO3	            PE3
IO4	            PE4
IO5	            PE5
IO6	            PE6
IO7	            PE7
IO8	            PB5
IO9	            PB4
IO10	        PB0/SSN
IO11	        PB2/MOSI
IO12	        PB3/MISO
IO13	        PB1/SCLK
AREF	        AREF
SDA	            PD1/SDA
SCL	            PD0/SCL
 */
HAL_GPIO_PIN(ARD_DIO,       D, 7);
HAL_GPIO_PIN(ARD_IO0,       E, 0);
HAL_GPIO_PIN(ARD_IO1,       E, 1);
HAL_GPIO_PIN(ARD_IO2,       E, 2);
HAL_GPIO_PIN(ARD_IO3,       E, 3);
HAL_GPIO_PIN(ARD_IO4,       E, 4);
HAL_GPIO_PIN(ARD_IO5,       E, 5);
HAL_GPIO_PIN(ARD_IO6,       E, 6);
HAL_GPIO_PIN(ARD_IO7,       E, 7);
//HAL_GPIO_PIN(ARD_IO8,       B, 5);    //For LoRa in IOH
//HAL_GPIO_PIN(ARD_IO9,       B, 4);
//HAL_GPIO_PIN(ARD_IO10,      B, 0);
//HAL_GPIO_PIN(ARD_IO11,      B, 2);
//HAL_GPIO_PIN(ARD_IO12,      B, 3);

void Arduino_IO_Init()
{
    HAL_GPIO_ARD_DIO_out();
    HAL_GPIO_ARD_DIO_set();

#ifdef _RS485_SENSOR_
//  HAL_GPIO_ARD_IO0_in();  //RXD0
//  HAL_GPIO_ARD_IO0_clr();

//  HAL_GPIO_ARD_IO1_out();  //TXD0
//  HAL_GPIO_ARD_IO1_clr();

//    HAL_GPIO_ARD_IO2_out();   //RS485_DE_nRE;  //Move to Uart0
//    HAL_GPIO_ARD_IO2_clr();

    HAL_GPIO_ARD_IO3_in();      //Rain Sensor
    HAL_GPIO_ARD_IO3_pullup();

    RELAY_ON;
#endif //_RS485_SENSOR_

    HAL_GPIO_ARD_IO5_out();
    HAL_GPIO_ARD_IO5_clr();

#ifdef _SHARP_PM25_SENSOR_
    HAL_GPIO_ARD_IO6_out();
    HAL_GPIO_ARD_IO6_set();     // Power/Fun Ctrl for PM25 Sensor (Active Low)

    HAL_GPIO_ARD_IO7_out();     // LED Ctrl of SHARP PM25 Sensor (Active Low)
    HAL_GPIO_ARD_IO7_set();
#else
    HAL_GPIO_ARD_IO6_out();
    HAL_GPIO_ARD_IO6_clr();

    HAL_GPIO_ARD_IO7_out();
    HAL_GPIO_ARD_IO7_clr();
#endif // _SHARP_PM25_SENSOR_

#ifdef _SENSOR_DHT22_
    InitDHTGpio();
#endif // _SENSOR_DHT22_

//    HAL_GPIO_ARD_IO8_out();
//    HAL_GPIO_ARD_IO8_clr();
//
//    HAL_GPIO_ARD_IO9_out();
//    HAL_GPIO_ARD_IO9_clr();
//
//    HAL_GPIO_ARD_IO10_out();
//    HAL_GPIO_ARD_IO10_clr();
//
//    HAL_GPIO_ARD_IO11_out();
//    HAL_GPIO_ARD_IO11_clr();
//
//    HAL_GPIO_ARD_IO12_out();
//    HAL_GPIO_ARD_IO12_clr();
}

HAL_GPIO_PIN(ARMEn,         B, 6);

/*- Types ------------------------------------------------------------------*/
typedef struct PACK
{
    // {Message definition start ...}
    uint8_t      commandId;
    uint8_t      nodeType;
    uint64_t     extAddr;
    uint16_t     shortAddr;
    uint32_t     softVersion;
    uint32_t     channelMask;
    uint16_t     panId;
    uint8_t      workingChannel;
    uint16_t     parentShortAddr;
    uint8_t      lqi;
    int8_t       rssi;

    struct PACK
    {
        uint8_t    type;
        uint8_t    size;
        int32_t    battery;
        int32_t    temperature;
        int32_t    light;
        int16_t    bmp180_temperature;
        uint32_t   bmp180_pressure;

        int16_t    acc_X;
        int16_t    acc_Y;
        int16_t    acc_Z;
        int16_t    magnetic_X;
        int16_t    magnetic_Y;
        int16_t    magnetic_Z;
        int16_t    acc2_X;
        int16_t    acc2_Y;
        int16_t    acc2_Z;
        int16_t    gyroscope_X;
        int16_t    gyroscope_Y;
        int16_t    gyroscope_Z;
        int16_t    ozone0;
        int16_t    ozone1;
        int16_t    ozone2;
        int16_t    ozone3;
        int16_t    ozone4;
        int16_t    ozone5;
        int16_t    AirTemperature;
        int16_t    AirHumidity;
    } sensors;

    struct PACK
    {
        uint8_t    type;
        uint8_t    size;
        char       text[APP_CAPTION_SIZE];
    } caption;
    // {... Message definition end}
} AppMessage_t;

typedef enum AppState_t
{
    APP_STATE_INITIAL,
    APP_STATE_SENSING,
    APP_STATE_WAIT_SENSOR_CONF,
    APP_STATE_SEND,
    APP_STATE_WAIT_CONF,
    APP_STATE_SENDING_DONE,
    APP_STATE_WAIT_SEND_TIMER,
    APP_STATE_WAIT_COMMAND_TIMER,
    APP_STATE_LORA_SEND,
    APP_STATE_WAIT_LORA_SEND_TIMER,
    APP_STATE_PREPARE_TO_SLEEP,
    APP_STATE_SLEEP,
    APP_STATE_WAKEUP,
} AppState_t;

/*- Variables --------------------------------------------------------------*/
static AppState_t appState = APP_STATE_INITIAL;

#ifdef NEO_Pixel
Adafruit_NeoPixel gNeoPixel = Adafruit_NeoPixel(12, 5, NEO_GRB + NEO_KHZ800);
#endif // NEO_Pixel

#if defined(APP_ROUTER) || defined(APP_ENDDEVICE)
static NWK_DataReq_t appNwkDataReq;
static SYS_Timer_t appNetworkStatusTimer;
static SYS_Timer_t appCommandWaitTimer;
static bool appNetworkStatus;
#endif

static AppMessage_t appMsg;
static SYS_Timer_t appDataSendingTimer;

/*- Implementations --------------------------------------------------------*/

/*************************************************************************//**
*****************************************************************************/
void HAL_UartBytesReceived(uint16_t bytes)
{
    for (uint16_t i = 0; i < bytes; i++)
    {
        uint8_t byte = HAL_UartReadByte();
        APP_CommandsByteReceived(byte);
    }
}

#ifdef _RS485_SENSOR_

/*************************************************************************//**
*****************************************************************************/
void appSoilPHSensorRead(uint16_t bytes)
{
    uint8_t ReceBuf[7];
    if(bytes >= 7)
    {
        for (uint16_t i = 0; i < 7; i++)
        {
            ReceBuf[i] = HAL_Uart0ReadByte();
        }
        uint16_t CRC = CRC16(ReceBuf, 5);
        uint8_t CRC_L = CRC & 0xff;
        uint8_t CRC_H = CRC >> 8;
        if( (ReceBuf[5] == CRC_L) && (ReceBuf[6] == CRC_H) )
        {
            appMsg.sensors.gyroscope_X = ReceBuf[3] * 256 + ReceBuf[4];
        }
        else
        {
            appMsg.sensors.gyroscope_X = 0xEEEE;
        }

        for (uint16_t i=7; i < bytes; i++)
        {
            ReceBuf[0] = HAL_Uart0ReadByte();
        }

        appSensingPollNextSensor();
    }
    else
    {
        appMsg.sensors.gyroscope_X = 30000 + bytes;
    }
}

/*************************************************************************//**
*****************************************************************************/
void appPM25SensorRead(uint16_t bytes)
{
    uint8_t ReceBuf[7];
    if(bytes >= 7)
    {
        for (uint16_t i = 0; i < 7; i++)
        {
            ReceBuf[i] = HAL_Uart0ReadByte();
        }
        uint16_t CRC = CRC16(ReceBuf, 5);
        uint8_t CRC_L = CRC & 0xff;
        uint8_t CRC_H = CRC >> 8;
        if( (ReceBuf[5] == CRC_L) && (ReceBuf[6] == CRC_H) )
        {
            appMsg.sensors.acc_X = ReceBuf[3] * 256 + ReceBuf[4];
        }
        else
        {
            appMsg.sensors.acc_X = 0xEEEE;
        }

        for (uint16_t i=7; i < bytes; i++)
        {
            ReceBuf[0] = HAL_Uart0ReadByte();
        }

        appSensingPollNextSensor();
    }
    else
    {
        appMsg.sensors.acc_X = 30000 + bytes;
    }
}

/*************************************************************************//**
*****************************************************************************/
void appCO2SensorRead(uint16_t bytes)
{
    uint8_t ReceBuf[7];
    if(bytes >= 7)
    {
        for (uint16_t i = 0; i < 7; i++)
        {
            ReceBuf[i] = HAL_Uart0ReadByte();
        }
        uint16_t CRC = CRC16(ReceBuf, 5);
        uint8_t CRC_L = CRC & 0xff;
        uint8_t CRC_H = CRC >> 8;
        if( (ReceBuf[5] == CRC_L) && (ReceBuf[6] == CRC_H) )
        {
            appMsg.sensors.acc_Y = ReceBuf[3] * 256 + ReceBuf[4];
        }
        else
        {
            appMsg.sensors.acc_Y = 0xEEEE;
        }

        for (uint16_t i=7; i < bytes; i++)
        {
            ReceBuf[0] = HAL_Uart0ReadByte();
        }

        appSensingPollNextSensor();
    }
    else
    {
        appMsg.sensors.acc_Y = 30000 + bytes;
    }
}

/*************************************************************************//**
*****************************************************************************/
void appTempHumSensorRead(uint16_t bytes)
{
    uint8_t ReceBuf[9];
    if(bytes >= 9)
    {
        for (uint16_t i = 0; i < 9; i++)
        {
            ReceBuf[i] = HAL_Uart0ReadByte();
        }
        uint16_t CRC = CRC16(ReceBuf, 7);
        uint8_t CRC_L = CRC & 0xff;
        uint8_t CRC_H = CRC >> 8;
        if( (ReceBuf[7] == CRC_L) && (ReceBuf[8] == CRC_H) )
        {
            appMsg.sensors.acc_Z = ReceBuf[3] * 256 + ReceBuf[4];
            appMsg.sensors.magnetic_X = ReceBuf[5] * 256 + ReceBuf[6];
        }
        else
        {
            appMsg.sensors.acc_Z = 0xEEEE;
            appMsg.sensors.magnetic_X = 0xEEEE;
        }

        for (uint16_t i=9; i < bytes; i++)
        {
            ReceBuf[0] = HAL_Uart0ReadByte();
        }

        appSensingPollNextSensor();
    }
    else
    {
        appMsg.sensors.acc_Z = 30000 + bytes;
        appMsg.sensors.magnetic_X = 30000 + bytes;
    }
}

/*************************************************************************//**
*****************************************************************************/
void appLightSensorRead(uint16_t bytes)
{
    uint8_t ReceBuf[9];
    if(bytes >= 9)
    {
        for (uint16_t i = 0; i < 9; i++)
        {
            ReceBuf[i] = HAL_Uart0ReadByte();
        }
        uint16_t CRC = CRC16(ReceBuf, 7);
        uint8_t CRC_L = CRC & 0xff;
        uint8_t CRC_H = CRC >> 8;
        if( (ReceBuf[7] == CRC_L) && (ReceBuf[8] == CRC_H) )
        {
            appMsg.sensors.magnetic_Y = ReceBuf[3] * 256 + ReceBuf[4];
            appMsg.sensors.magnetic_Z = ReceBuf[5] * 256 + ReceBuf[6];
        }
        else
        {
            appMsg.sensors.magnetic_Y = 0xEEEE;
            appMsg.sensors.magnetic_Z = 0xEEEE;
        }

        for (uint16_t i=9; i < bytes; i++)
        {
            ReceBuf[0] = HAL_Uart0ReadByte();
        }

        appSensingPollNextSensor();
    }
    else
    {
        appMsg.sensors.magnetic_Y = 30000 + bytes;
        appMsg.sensors.magnetic_Z = 30000 + bytes;
    }
}

/*************************************************************************//**
*****************************************************************************/
void appSoilTriInfoSensorRead(uint16_t bytes)
{
    uint8_t ReceBuf[7+4];
    if(bytes >= 7+4)
    {
        for (uint16_t i = 0; i < 7+4; i++)
        {
            ReceBuf[i] = HAL_Uart0ReadByte();
        }
        uint16_t CRC = CRC16(ReceBuf, 5+4);
        uint8_t CRC_L = CRC & 0xff;
        uint8_t CRC_H = CRC >> 8;
        if( (ReceBuf[5+4] == CRC_L) && (ReceBuf[6+4] == CRC_H) )
        {
            appMsg.sensors.acc2_X = ReceBuf[3] * 256 + ReceBuf[4];
            appMsg.sensors.acc2_Y = ReceBuf[3+2] * 256 + ReceBuf[4+2];
            appMsg.sensors.acc2_Z = ReceBuf[3+4] * 256 + ReceBuf[4+4];
        }
        else
        {
            appMsg.sensors.acc2_X = 0xEEEE;
            appMsg.sensors.acc2_Y = 0xEEEE;
            appMsg.sensors.acc2_Z = 0xEEEE;
        }

        for (uint16_t i=7+4; i < bytes; i++)
        {
            ReceBuf[0] = HAL_Uart0ReadByte();
        }

        appSensingPollNextSensor();
    }
    else
    {
        appMsg.sensors.acc2_X = 30000 + bytes;
        appMsg.sensors.acc2_Y = 30000 + bytes;
        appMsg.sensors.acc2_Z = 30000 + bytes;
    }
}
#endif //_RS485_SENSOR_

#ifdef _COZIR_CO2_SENSOR_
void appCOZIRCO2SensorRead(uint16_t bytes)
{
    uint8_t ReceBuf[7];
    while(bytes >= 7)
    {
        ReceBuf[0] = HAL_Uart0ReadByte();
        if( (ReceBuf[0] == 'Z') )
        {
            for (uint16_t i = 1; i < 7; i++)
            {
                ReceBuf[i] = HAL_Uart0ReadByte();
            }

            appMsg.sensors.acc_Y =
                (ReceBuf[2]-'0') * 10000 +
                (ReceBuf[3]-'0') * 1000 +
                (ReceBuf[4]-'0') * 100 +
                (ReceBuf[5]-'0') * 10 +
                (ReceBuf[6]-'0');
            appMsg.sensors.acc_Z ++;
            appMsg.sensors.acc_X += 7;
            bytes -=7;
        }
        else
        {
            appMsg.sensors.acc_X ++;
            bytes --;
        }
    }
}
#endif // _COZIR_CO2_SENSOR_

#ifdef _YW51_PM25_SENSOR_
void appYW51PM25SensorRead(uint16_t bytes)
{
    uint8_t ReceBuf[7];
    if(bytes >= 7)
    {
        for (uint16_t i = 0; i < 7; i++)
        {
            ReceBuf[i] = HAL_Uart0ReadByte();
            appMsg.sensors.acc_X += 7;
        }

        while(bytes >= 7)
        {
            if( (ReceBuf[0] == 0xAA) && (ReceBuf[6] == 0XFF) )
            {
                appMsg.sensors.acc_Y = (ReceBuf[1]) * 256 + (ReceBuf[2]);
                appMsg.sensors.acc_Z ++;
                bytes -=7;
            }
            else
            {
                bytes --;
                if(bytes >= 7)
                {
                    memmove(&ReceBuf[0], &ReceBuf[1], 6);
                    ReceBuf[6] = HAL_Uart0ReadByte();
                    appMsg.sensors.acc_X ++;
                }

            }
        }
    }

}
#endif // _YW51_PM25_SENSOR_

#ifdef HAL_Uart0_Actived
/*************************************************************************//**
*****************************************************************************/
void HAL_Uart0BytesReceived(uint16_t bytes)
{
#ifdef _RS485_SENSOR_
    if(g_RS485Sensor_Handler)
        (g_RS485Sensor_Handler)(bytes);
#endif //_RS485_SENSOR_
#ifdef _COZIR_CO2_SENSOR_
    appCOZIRCO2SensorRead(bytes);
#endif // _COZIR_CO2_SENSOR_
#ifdef _YW51_PM25_SENSOR_
    appYW51PM25SensorRead(bytes);
#endif // _YW51_PM25_SENSOR_

    (void)(bytes); // Unused

}
#endif // HAL_Uart0_Actived

/*************************************************************************//**
*****************************************************************************/
static void appUartSendMessage(uint8_t *data, uint8_t size)
{
    uint8_t cs = 0;

    HAL_UartWriteByte(0x10);
    HAL_UartWriteByte(0x02);

    for (uint8_t i = 0; i < size; i++)
    {
        if (data[i] == 0x10)
        {
            HAL_UartWriteByte(0x10);
            cs += 0x10;
        }
        HAL_UartWriteByte(data[i]);
        cs += data[i];
    }

    HAL_UartWriteByte(0x10);
    HAL_UartWriteByte(0x03);
    cs += 0x10 + 0x02 + 0x10 + 0x03;

    HAL_UartWriteByte(cs);
}

#ifdef LORA_433MHZ
static AppMessage_t appLoRaMsg;
void fqcRecvData(unsigned char *lpbuf,unsigned short len)// ���յ�RF����
{
    appMsg.softVersion += len;
    appMsg.sensors.gyroscope_Z ++;
#ifdef RGB_LCD_RXDATA
    halLCD_PrintLoRaFrame(lpbuf, len);
#endif // RGB_LCD

    memset(&appLoRaMsg, 0, sizeof(appLoRaMsg));
    appLoRaMsg.commandId = 100;

    appLoRaMsg.rssi = PKTRSSIVALUE;
    appLoRaMsg.lqi = PKTSNRVALUE;
    appLoRaMsg.sensors.type = RSSIVALUE;

    appLoRaMsg.sensors.size = len;

    appLoRaMsg.shortAddr = lpbuf[0];
    appLoRaMsg.softVersion = (((uint32_t)lpbuf[1]) << 24)
                | (((uint32_t)lpbuf[2]) << 16)
                | (((uint32_t)lpbuf[3]) << 8)
                | (((uint32_t)lpbuf[4])) ;

    appLoRaMsg.caption.type         = 32;
    appLoRaMsg.caption.size         = 3;
    memcpy(appLoRaMsg.caption.text, lpbuf+5, 3);

    appUartSendMessage((uint8_t*)&appLoRaMsg, sizeof(appLoRaMsg));

    App_SX1278_DumpReg();
    SX1278_Reg[0] = 102;
    appUartSendMessage(SX1278_Reg, 128);
}

static void appLoRaSendTimerHandler(SYS_Timer_t *timer)
{
    if(appState == APP_STATE_WAIT_LORA_SEND_TIMER)
    {
        appState = APP_STATE_PREPARE_TO_SLEEP;
    }
    (void)timer;
}

void fTransmitFin(void)
{
    App_SX1278_DumpReg();
    SX1278_Reg[0] = 103;
    appUartSendMessage(SX1278_Reg, 128);

    if(appState == APP_STATE_WAIT_LORA_SEND_TIMER)
    {
        SYS_TimerStop(&appLoRaSendTimer);

        appState = APP_STATE_PREPARE_TO_SLEEP;
    }
}
#endif //LORA_433MHZ

#ifdef _RS485_SENSOR_

/*************************************************************************//**
*****************************************************************************/
uint16_t CRC16 (const uint8_t *nData, uint16_t wLength)
{
    static const uint16_t wCRCTable[] = {
    0X0000, 0XC0C1, 0XC181, 0X0140, 0XC301, 0X03C0, 0X0280, 0XC241,
    0XC601, 0X06C0, 0X0780, 0XC741, 0X0500, 0XC5C1, 0XC481, 0X0440,
    0XCC01, 0X0CC0, 0X0D80, 0XCD41, 0X0F00, 0XCFC1, 0XCE81, 0X0E40,
    0X0A00, 0XCAC1, 0XCB81, 0X0B40, 0XC901, 0X09C0, 0X0880, 0XC841,
    0XD801, 0X18C0, 0X1980, 0XD941, 0X1B00, 0XDBC1, 0XDA81, 0X1A40,
    0X1E00, 0XDEC1, 0XDF81, 0X1F40, 0XDD01, 0X1DC0, 0X1C80, 0XDC41,
    0X1400, 0XD4C1, 0XD581, 0X1540, 0XD701, 0X17C0, 0X1680, 0XD641,
    0XD201, 0X12C0, 0X1380, 0XD341, 0X1100, 0XD1C1, 0XD081, 0X1040,
    0XF001, 0X30C0, 0X3180, 0XF141, 0X3300, 0XF3C1, 0XF281, 0X3240,
    0X3600, 0XF6C1, 0XF781, 0X3740, 0XF501, 0X35C0, 0X3480, 0XF441,
    0X3C00, 0XFCC1, 0XFD81, 0X3D40, 0XFF01, 0X3FC0, 0X3E80, 0XFE41,
    0XFA01, 0X3AC0, 0X3B80, 0XFB41, 0X3900, 0XF9C1, 0XF881, 0X3840,
    0X2800, 0XE8C1, 0XE981, 0X2940, 0XEB01, 0X2BC0, 0X2A80, 0XEA41,
    0XEE01, 0X2EC0, 0X2F80, 0XEF41, 0X2D00, 0XEDC1, 0XEC81, 0X2C40,
    0XE401, 0X24C0, 0X2580, 0XE541, 0X2700, 0XE7C1, 0XE681, 0X2640,
    0X2200, 0XE2C1, 0XE381, 0X2340, 0XE101, 0X21C0, 0X2080, 0XE041,
    0XA001, 0X60C0, 0X6180, 0XA141, 0X6300, 0XA3C1, 0XA281, 0X6240,
    0X6600, 0XA6C1, 0XA781, 0X6740, 0XA501, 0X65C0, 0X6480, 0XA441,
    0X6C00, 0XACC1, 0XAD81, 0X6D40, 0XAF01, 0X6FC0, 0X6E80, 0XAE41,
    0XAA01, 0X6AC0, 0X6B80, 0XAB41, 0X6900, 0XA9C1, 0XA881, 0X6840,
    0X7800, 0XB8C1, 0XB981, 0X7940, 0XBB01, 0X7BC0, 0X7A80, 0XBA41,
    0XBE01, 0X7EC0, 0X7F80, 0XBF41, 0X7D00, 0XBDC1, 0XBC81, 0X7C40,
    0XB401, 0X74C0, 0X7580, 0XB541, 0X7700, 0XB7C1, 0XB681, 0X7640,
    0X7200, 0XB2C1, 0XB381, 0X7340, 0XB101, 0X71C0, 0X7080, 0XB041,
    0X5000, 0X90C1, 0X9181, 0X5140, 0X9301, 0X53C0, 0X5280, 0X9241,
    0X9601, 0X56C0, 0X5780, 0X9741, 0X5500, 0X95C1, 0X9481, 0X5440,
    0X9C01, 0X5CC0, 0X5D80, 0X9D41, 0X5F00, 0X9FC1, 0X9E81, 0X5E40,
    0X5A00, 0X9AC1, 0X9B81, 0X5B40, 0X9901, 0X59C0, 0X5880, 0X9841,
    0X8801, 0X48C0, 0X4980, 0X8941, 0X4B00, 0X8BC1, 0X8A81, 0X4A40,
    0X4E00, 0X8EC1, 0X8F81, 0X4F40, 0X8D01, 0X4DC0, 0X4C80, 0X8C41,
    0X4400, 0X84C1, 0X8581, 0X4540, 0X8701, 0X47C0, 0X4680, 0X8641,
    0X8201, 0X42C0, 0X4380, 0X8341, 0X4100, 0X81C1, 0X8081, 0X4040 };

    uint8_t nTemp;
    uint16_t wCRCWord = 0xFFFF;

   while (wLength--)
   {
      nTemp = *nData++ ^ wCRCWord;
      wCRCWord >>= 8;
      wCRCWord ^= wCRCTable[nTemp];
   }
   return wCRCWord;

}

/*************************************************************************//**
*****************************************************************************/
static void appUart0SendMessage(const uint8_t *data, uint8_t size)
{
    _delay_ms(10);

    HAL_Uart0ClearRxBuf();

    uint16_t CRC = CRC16(data, size);
    for (uint8_t i = 0; i < size; i++)
    {
        HAL_Uart0WriteByte(data[i]);
    }
    HAL_Uart0WriteByte(CRC & 0xff);
    HAL_Uart0WriteByte(CRC >> 8);
}

#endif //_RS485_SENSOR_

/*************************************************************************//**
*****************************************************************************/
static bool appDataInd(NWK_DataInd_t *ind)
{
    uint8_t i, j;

	//unused i, j
	(void)(i);
	(void)(j);

    AppMessage_t *msg = (AppMessage_t *)ind->data;

    // HAL_LedToggle(0);

    msg->lqi = ind->lqi;
    msg->rssi = ind->rssi;

  if(msg->shortAddr < 0x8000)
    g_nSensPacketCnt[1] ++;
  else if(msg->shortAddr >= 0x8000)
    g_nSensPacketCnt[2] ++;

#ifdef NEO_Pixel
    for(j=0; j<3; j++)
    {
        for(i=0; i<(g_nSensPacketCnt[j]%13); i++)
        {
			uint32_t color = gNeoPixel.getPixelColor(i);
			if(j==0)
			{
				color &= 0xffff00;
				color |= 0x000010;
			}
			else if(j==1)
			{
				color &= 0xff00ff;
				color |= 0x001000;
			}
			else if(j==2)
			{
				color &= 0x00ffff;
				color |= 0x100000;
			}
            // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
            gNeoPixel.setPixelColor(i, color); // Moderately bright green color.
        }
        for(; i<12; i++)
        {
			uint32_t color = gNeoPixel.getPixelColor(i);
			if(j==0)
			{
				color &= 0xffff00;
//				color |= 0x000020;
			}
			else if(j==1)
			{
				color &= 0xff00ff;
//				color |= 0x002000;
			}
			else if(j==2)
			{
				color &= 0x00ffff;
//				color |= 0x200000;
			}            // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
            gNeoPixel.setPixelColor(i, color); // Moderately bright green color.
        }
    }


    gNeoPixel.show(); // This sends the updated pixel color to the hardware.
#endif // NEO_Pixel

    appUartSendMessage(ind->data, ind->size);
#ifndef LORA_433MHZ
#endif //LORA_433MHZ

    if (APP_CommandsPending(ind->srcAddr))
    {
        NWK_SetAckControl(APP_COMMAND_PENDING);
    }

    return true;
}

/*************************************************************************//**
*****************************************************************************/
static void appDataSendingTimerHandler(SYS_Timer_t *timer)
{
    if (APP_STATE_WAIT_SEND_TIMER == appState)
    {
        appState = APP_STATE_SENSING;
    }
    else
    {
        SYS_TimerStart(&appDataSendingTimer);
    }

    (void)timer;
}

#if defined(APP_ROUTER) || defined(APP_ENDDEVICE)
/*************************************************************************//**
*****************************************************************************/
static void appNetworkStatusTimerHandler(SYS_Timer_t *timer)
{
    // HAL_LedToggle(APP_LED_NETWORK);
    (void)timer;
}

/*************************************************************************//**
*****************************************************************************/
static void appCommandWaitTimerHandler(SYS_Timer_t *timer)
{
    appState = APP_STATE_SENDING_DONE;
    (void)timer;
}
#endif

/*************************************************************************//**
*****************************************************************************/
#if defined(APP_ROUTER) || defined(APP_ENDDEVICE)
static void appDataConf(NWK_DataReq_t *req)
{
    // HAL_LedOff(0);

    if (NWK_SUCCESS_STATUS == req->status)
    {
        if (!appNetworkStatus)
        {
            // HAL_LedOn(APP_LED_NETWORK);
            SYS_TimerStop(&appNetworkStatusTimer);
            appNetworkStatus = true;
        }
    }
    else
    {
        if (appNetworkStatus)
        {
            // HAL_LedOff(APP_LED_NETWORK);
            SYS_TimerStart(&appNetworkStatusTimer);
            appNetworkStatus = false;
        }
    }

    if (APP_COMMAND_PENDING == req->control)
    {
        SYS_TimerStart(&appCommandWaitTimer);
        appState = APP_STATE_WAIT_COMMAND_TIMER;
    }
    else
    {
        appState = APP_STATE_SENDING_DONE;
    }
}
#endif



void Read_lsm303dlhc(void)
{
    uint8_t response;
    AccAxesRaw_t data;
    MagAxesRaw_t dataM;

    response = GetAccAxesRaw(&data);

    if (response == 1)
    {
        appMsg.sensors.acc_X = data.AXIS_X;
        appMsg.sensors.acc_Y = data.AXIS_Y;
        appMsg.sensors.acc_Z = data.AXIS_Z;
    }

    response = GetMagAxesRaw(&dataM);
    if (response == 1)
    {
        appMsg.sensors.magnetic_X = dataM.AXIS_X;
        appMsg.sensors.magnetic_Y = dataM.AXIS_Y;
        appMsg.sensors.magnetic_Z = dataM.AXIS_Z;
    }

}

void LSM330DLC_lsm303dlhc_Init(void)
{
    lsm303dlhc_init();

    LSM330_ACC_init();
	LSM330_Gyro_init();

}


void Read_LSM330DLC(void)
{
    uint8_t response;
    AccAxesRaw_t data;
    GyroAxesRaw_t dataG;

    //LSM330_ACC_init();

    response = LSM330_GetAccAxesRaw(&data);

    if (response == 1)
    {
        appMsg.sensors.acc2_X = data.AXIS_X;
        appMsg.sensors.acc2_Y = data.AXIS_Y;
        appMsg.sensors.acc2_Z = data.AXIS_Z;
    }

    //LSM330_Gyro_init();

    response = LSM330_GetGyroAxesRaw(&dataG);
    if (response == 1)
    {
        appMsg.sensors.gyroscope_X = dataG.AXIS_X;
        appMsg.sensors.gyroscope_Y = dataG.AXIS_Y;
        appMsg.sensors.gyroscope_Z = dataG.AXIS_Z;
    }

}

#ifdef _RS485_SENSOR_

/*************************************************************************//**
*****************************************************************************/
static void appDataSensingTimerHandler(SYS_Timer_t *timer)
{
    if(appState == APP_STATE_WAIT_SENSOR_CONF)
    {
        g_CurRS485SensorIndex ++;
        appSensingSendCmd();

        appMsg.sensors.gyroscope_Y ++;
    }
    (void)timer;
}

#define ACTIVE_12V_SENSING_CNT (30)             // Active Sample Count
#define TOTAL_PERIOD_12V_SENSING_CNT (6*20)    // Total Sample Count
#define ACTIVE_12V_MAX_TIME (145)               // Active Period Sec
#define TOTAL_PERIOD_12V_MAX_TIME (600)       // Total Period Sec

/*************************************************************************//**
*****************************************************************************/
static void appSensingData(void)
{
	static uint16_t nDiv = 0; // 0-2Min, +12V always ON, 2Min-6H, +12V OFF
	static uint32_t nInitSecTick = 0;
    uint32_t nCurTick = SYS_GetSysTick();
    uint32_t nCurSecTick = nCurTick >> 10;

    appMsg.sensors.ozone0 = nCurTick >> 16;
    appMsg.sensors.ozone1 = nCurTick & 0xffff;
    appMsg.sensors.ozone3 = nCurSecTick & 0xffff;

    appSensingInitRS485Com();

	if(nDiv == 0) //Init
    {
#ifdef  POWER_ON_OFF_ARDIO
        HAL_GPIO_ARD_DIO_out();
        HAL_GPIO_ARD_DIO_set();     //ARD DIO En
#endif // POWER_ON_OFF_ARDIO

        RELAY_ON;

        nInitSecTick = nCurSecTick;

        _delay_ms(2000);
    }

	if(nDiv < ACTIVE_12V_SENSING_CNT) // * 20Sec
    {
        if( (nCurSecTick >= nInitSecTick) && (nCurSecTick < (nInitSecTick+ACTIVE_12V_MAX_TIME)) )
        {
            appMsg.sensors.ozone2 = 100;

#ifdef  POWER_ON_OFF_ARDIO
            HAL_GPIO_ARD_DIO_out();
            HAL_GPIO_ARD_DIO_set();     //ARD DIO En
#endif // POWER_ON_OFF_ARDIO

            RELAY_ON;

            appSensingSendCmd();
        }
        else if( (nCurSecTick < nInitSecTick)  && ((nCurSecTick+0x400000) < (nInitSecTick+ACTIVE_12V_MAX_TIME)) )
        {
            appMsg.sensors.ozone2 = 200;

#ifdef  POWER_ON_OFF_ARDIO
            HAL_GPIO_ARD_DIO_out();
            HAL_GPIO_ARD_DIO_set();     //ARD DIO En
#endif // POWER_ON_OFF_ARDIO

            RELAY_OFF;

            appSensingSendCmd();
        }
        else
        {
            appMsg.sensors.ozone2 = 300;

            RELAY_OFF;

#ifdef  POWER_ON_OFF_ARDIO
            HAL_GPIO_ARD_DIO_out();
            HAL_GPIO_ARD_DIO_clr();     //ARD DIO Off
#endif // POWER_ON_OFF_ARDIO

            appState = APP_STATE_SEND;
        }
    }
    else
    {
        appMsg.sensors.ozone2 = 400;

        RELAY_OFF;

#ifdef  POWER_ON_OFF_ARDIO
        HAL_GPIO_ARD_DIO_out();
        HAL_GPIO_ARD_DIO_clr();     //ARD DIO Off
#endif // POWER_ON_OFF_ARDIO

        appState = APP_STATE_SEND;
    }

    appMsg.sensors.ozone4 = nDiv;
    nDiv ++;
    if(nDiv >= TOTAL_PERIOD_12V_SENSING_CNT)
        nDiv = 0;
}

/*************************************************************************//**
*****************************************************************************/
void appSensingInitRS485Com(void)
{
    g_CurRS485SensorIndex = 0;
    for(uint8_t i=0; i<g_RS485_Sensor_Cnt; i++)
    {
        if(g_RS485_Sensor[i].Sensor_Response_Handler)
            (g_RS485_Sensor[i].Sensor_Response_Handler)(0);
    }
}

/*************************************************************************//**
*****************************************************************************/
void appSensingSendCmd(void)
{
    if(g_CurRS485SensorIndex < g_RS485_Sensor_Cnt)
    {
        appUart0SendMessage(g_RS485_Sensor[g_CurRS485SensorIndex].Cmd, g_RS485_Sensor[g_CurRS485SensorIndex].CmdLen);
        g_RS485Sensor_Handler = g_RS485_Sensor[g_CurRS485SensorIndex].Sensor_Response_Handler;

        appSensingTimer.interval = 2000;
        appSensingTimer.mode = SYS_TIMER_INTERVAL_MODE;
        appSensingTimer.handler = appDataSensingTimerHandler;
        SYS_TimerStart(&appSensingTimer);

        appState = APP_STATE_WAIT_SENSOR_CONF;
    }
    else
    {
        g_CurRS485SensorIndex = 0;

        appState = APP_STATE_SEND;
    }
}

/*************************************************************************//**
*****************************************************************************/
void appSensingPollNextSensor(void)
{
    SYS_TimerStop(&appSensingTimer);
    g_CurRS485SensorIndex ++;
    appSensingSendCmd();
}

#endif //_RS485_SENSOR_

/*************************************************************************//**
*****************************************************************************/
static void appSendData(void)
{
    uint8_t i, j;
	static uint8_t nDiv = 0;

#ifdef NWK_ENABLE_ROUTING
    appMsg.parentShortAddr = NWK_RouteNextHop(0, 0);
#else
    appMsg.parentShortAddr = 0;
#endif

#ifdef _RS485_SENSOR_
    if(appMsg.sensors.ozone4 >= ACTIVE_12V_SENSING_CNT)
    {
        RELAY_OFF;

#ifdef  POWER_ON_OFF_ARDIO
        HAL_GPIO_ARD_DIO_out();
        HAL_GPIO_ARD_DIO_clr();     //ARD DIO Off
#endif // POWER_ON_OFF_ARDIO
    }
    else
    {
        appMsg.sensors.ozone5  = HAL_GPIO_ARD_IO3_read();
    }
#endif //_RS485_SENSOR_

#if defined(FIX_ON_OFF_ARDIO)
    HAL_GPIO_ARD_DIO_out();
    HAL_GPIO_ARD_DIO_set();     //ARD DIO En
#endif // defined(FIX_ON_OFF_ARDIO)

#ifdef _SENSOR_DS18B20_
    {
        HAL_GPIO_OneWirePWR_out();
        HAL_GPIO_OneWirePWR_set();

        HAL_GPIO_OneWireD_out();
        HAL_GPIO_OneWireD_set();
    }
#endif // _SENSOR_DS18B20_

#ifdef _SHARP_PM25_SENSOR_
    SHARP_Sensor_ON;
    SHARP_LED_OFF;
    {
        uint16_t temp;
        for(temp=0; temp<2000; temp++)
        {
            _delay_us(200);
        }
    }
#endif // _SHARP_PM25_SENSOR_

    sensor_power_on();

	adc_init();
	appMsg.sensors.battery     = adc_read(0); // rand() & 0xffff;
	appMsg.sensors.light       = adc_read(1); //rand() & 0xff;
#ifdef _RS485_SENSOR_
	// appMsg.sensors.ozone0       = adc_read(4); //rand() & 0xff;     // Change to Tick_H
	// appMsg.sensors.ozone1       = adc_read(5); //rand() & 0xff;     // Change to Tick_L
	// appMsg.sensors.ozone2       = adc_read(6); //rand() & 0xff;     // Change to RS485 Active State
	// appMsg.sensors.ozone3       = adc_read(7); //rand() & 0xff;     // SecTick
    // appMsg.sensors.ozone4       = adc_read(2); //rand() & 0xff;     //Change to Sensor Div
	// appMsg.sensors.ozone5       = adc_read(3); //rand() & 0xff;     //Change to Rain Sensor
#else
    appMsg.sensors.ozone0       = adc_read(4); //rand() & 0xff;     // Change Order for new uSu_Edu
	appMsg.sensors.ozone1       = adc_read(5); //rand() & 0xff;
	appMsg.sensors.ozone2       = adc_read(6); //rand() & 0xff;
	appMsg.sensors.ozone3       = adc_read(7); //rand() & 0xff;
    appMsg.sensors.ozone4       = adc_read(2); //rand() & 0xff;
	appMsg.sensors.ozone5       = adc_read(3); //rand() & 0xff;
#endif //_RS485_SENSOR_

#ifdef _SHARP_PM25_SENSOR_
    for(i=0; i<10; i++)
    {
        SHARP_LED_ON;
        _delay_us(160);
        _delay_us(160);
        SHARP_LED_OFF;
        for(j=0; j<80; j++)
            _delay_us(110);
    }
    SHARP_LED_ON;
    _delay_us(160);
    _delay_us(120);
    appMsg.sensors.ozone4       = adc_read(2); //rand() & 0xff;
    SHARP_LED_OFF;
    SHARP_Sensor_OFF;
#endif // _SHARP_PM25_SENSOR_

	adc_close();

#ifdef LORA_433MHZ
    appMsg.sensors.gyroscope_Z ++;
#else
    appMsg.sensors.gyroscope_Z ++;
#endif // LORA_433MHZ

#ifdef _SENSOR_DHT22_
//    InitDHTGpio();

//    appMsg.sensors.gyroscope_X = dht.read(1);
//    appMsg.sensors.AirTemperature = (((uint16_t)dht.data[2])<<8) | (((uint16_t)dht.data[3])<<2);
//    appMsg.sensors.AirHumidity = (((uint16_t)dht.data[0])<<8) | (((uint16_t)dht.data[1])<<2);
//    appMsg.sensors.gyroscope_Y = dht.data[4];
    appMsg.sensors.AirTemperature = (int16_t)(dht.readTemperature(0, 1) * 10.0);
    appMsg.sensors.AirHumidity = (int16_t)(dht.readHumidity() * 10.0);

//    DeInitDHTGpio();
#endif // _SENSOR_DHT22_

#ifdef _SENSOR_DS18B20_
    {
        int temp;

        //Start conversion (without ROM matching)
        ds18b20convert( &PORTD, &DDRD, &PIND, ( 1 << 7 ), NULL );

        //Delay (sensor needs time to perform conversion)
        _delay_ms( 100 );

        //Read temperature (without ROM matching)
        ds18b20read( &PORTD, &DDRD, &PIND, ( 1 << 7 ), NULL, &temp );

        appMsg.sensors.gyroscope_X = temp;

        HAL_GPIO_OneWireD_out();
        HAL_GPIO_OneWireD_clr();

        HAL_GPIO_OneWirePWR_out();
        HAL_GPIO_OneWirePWR_clr();
    }
#endif // _SENSOR_DS18B20_

#ifdef ALPHASENSE_END
    (void)(i); // Unused
    (void)(j); // Unused
    (void)(nDiv); // Unused

#ifdef _SENSOR_LM75_
    lm75_init();
    short temp = 1;
    uint8_t count = 5;
    do
    {
        temp = lm75_read_temperature();
        count--;
        _delay_ms(10);
    }
    while (temp == 1 && count > 0);
    appMsg.sensors.temperature = temp;
#endif

    sensor_power_off();

    Clr_SDA;  // for TWI I2C, we cannot directly close SDA
    Clr_SCL;

#else

#if defined(FIX_ON_OFF_ARDIO)
    bmp180_init();

	LSM330DLC_lsm303dlhc_Init();
#endif // defined(FIX_ON_OFF_ARDIO)

    g_nSensPacketCnt[0] ++;

#ifdef NEO_Pixel
    for(j=0; j<3; j++)
    {
        for(i=0; i<(g_nSensPacketCnt[j]%13); i++)
        {
			uint32_t color = gNeoPixel.getPixelColor(i);
			if(j==0)
			{
				color &= 0xffff00;
				color |= 0x000010;
			}
			else if(j==1)
			{
				color &= 0xff00ff;
				color |= 0x001000;
			}
			else if(j==2)
			{
				color &= 0x00ffff;
				color |= 0x100000;
			}
            // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
            gNeoPixel.setPixelColor(i, color); // Moderately bright green color.
        }
        for(; i<12; i++)
        {
			uint32_t color = gNeoPixel.getPixelColor(i);
			if(j==0)
			{
				color &= 0xffff00;
//				color |= 0x000020;
			}
			else if(j==1)
			{
				color &= 0xff00ff;
//				color |= 0x002000;
			}
			else if(j==2)
			{
				color &= 0x00ffff;
//				color |= 0x200000;
			}            // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
            gNeoPixel.setPixelColor(i, color); // Moderately bright green color.
        }
    }


    gNeoPixel.show(); // This sends the updated pixel color to the hardware.

#endif // NEO_Pixel

	if(!nDiv)
	{
#ifdef _SENSOR_LM75_
        lm75_init();
	    short temp = 1;
	    uint8_t count = 5;
	    do
	    {
	        temp = lm75_read_temperature();
	        count--;
	        _delay_ms(10);
	    }
	    while (temp == 1 && count > 0);
	    appMsg.sensors.temperature = temp;
	    // appMsg.sensors.temperature = lm75_read_temperature(); //rand() & 0x7f;
#endif
	    Read_lsm303dlhc();
	    Read_LSM330DLC();


	    appMsg.sensors.bmp180_temperature = bmp180_read_template();
	    appMsg.sensors.bmp180_pressure = bmp180_read_pressure();

 #ifdef _SENSOR_SHT1X_
       {
            uint8_t msb, lsb ,chksum;
            sht1x_init();
            sht1x_softReset();

            sht1x_readTemperature(&msb, &lsb, &chksum);
            appMsg.sensors.AirTemperature = (((uint16_t)msb)<<10) | (((uint16_t)lsb)<<2);

            sht1x_readHumidity(&msb, &lsb, &chksum);
            appMsg.sensors.AirHumidity = (((uint16_t)msb)<<12) | (((uint16_t)lsb)<<4);
        }
#endif

    }
	else
	{
		Read_LSM330DLC();
	}
//	nDiv ++;

#endif //ALPHASENSE_END

#if defined(FIX_ON_OFF_ARDIO)
    sensor_power_off();

    Clr_SDA;  // for TWI I2C, we cannot directly close SDA
    Clr_SCL;

    HAL_GPIO_ARD_DIO_out();
    HAL_GPIO_ARD_DIO_clr();     //ARD DIO Off
#endif // defined(FIX_ON_OFF_ARDIO)

#if defined(APP_COORDINATOR)

    appUartSendMessage((uint8_t *)&appMsg, sizeof(appMsg));
#ifndef LORA_433MHZ
#endif //LORA_433MHZ

    SYS_TimerStart(&appDataSendingTimer);
    appState = APP_STATE_WAIT_SEND_TIMER;

#ifndef ACTIVE_LED
    HAL_LedOff(1);
#else
    HAL_LedToggle(1);
#endif

#else
    appUartSendMessage((uint8_t *)&appMsg, sizeof(appMsg));  //also output for end device

    appNwkDataReq.dstAddr = 0;
    appNwkDataReq.dstEndpoint = APP_ENDPOINT;
    appNwkDataReq.srcEndpoint = APP_ENDPOINT;
    appNwkDataReq.options = NWK_OPT_ACK_REQUEST | NWK_OPT_ENABLE_SECURITY;
    appNwkDataReq.data = (uint8_t *)&appMsg;
    appNwkDataReq.size = sizeof(appMsg);
    appNwkDataReq.confirm = appDataConf;

#ifndef ACTIVE_LED
    HAL_LedOff(1);
#else
    HAL_LedToggle(1);
#endif

    NWK_DataReq(&appNwkDataReq);

    appState = APP_STATE_WAIT_CONF;
#endif
}

/*************************************************************************//**
*****************************************************************************/
static void appInit(void)
{
    appMsg.commandId            = APP_COMMAND_ID_NETWORK_INFO;
    appMsg.nodeType             = APP_NODE_TYPE;
    appMsg.extAddr              = APP_ADDR;
    appMsg.shortAddr            = APP_ADDR;
#ifdef LORA_433MHZ
    appMsg.softVersion          = 0x0;
#else
    appMsg.softVersion          = 0x01010100;
#endif // LORA_433MHZ
    appMsg.channelMask          = (1L << APP_CHANNEL);
    appMsg.panId                = APP_PANID;
    appMsg.workingChannel       = APP_CHANNEL;
    appMsg.parentShortAddr      = 0;
    appMsg.lqi                  = 0;
    appMsg.rssi                 = 0;

    appMsg.sensors.type        = 1;
    appMsg.sensors.size        = sizeof(int32_t) * 3;
    appMsg.sensors.battery     = 0;
    appMsg.sensors.temperature = 0;
    appMsg.sensors.light       = 0;
    appMsg.sensors.bmp180_temperature = 0;
    appMsg.sensors.bmp180_pressure    = 0;

    appMsg.sensors.acc_X       = 0;
    appMsg.sensors.acc_Y       = 0;
    appMsg.sensors.acc_Z       = 0;
    appMsg.sensors.magnetic_X  = 0;
    appMsg.sensors.magnetic_Y  = 0;
    appMsg.sensors.magnetic_Z  = 0;
    appMsg.sensors.acc2_X      = 0;
    appMsg.sensors.acc2_Y      = 0;
    appMsg.sensors.acc2_Z      = 0;
    appMsg.sensors.gyroscope_X = 0;
    appMsg.sensors.gyroscope_Y = 0;
    appMsg.sensors.gyroscope_Z = 0;

    appMsg.caption.type         = 32;
    appMsg.caption.size         = APP_CAPTION_SIZE;
    memcpy(appMsg.caption.text, APP_CAPTION, APP_CAPTION_SIZE);

    HAL_BoardInit();
    // HAL_LedInit();

    sensor_power_on();

//  HAL_GPIO_ARD_DIO_out();
//  HAL_GPIO_ARD_DIO_set();
    Arduino_IO_Init();

    HAL_GPIO_ARMEn_out();
    HAL_GPIO_ARMEn_clr();

    Set_SDA;
    Set_SCL;
    halI2C_Init();

#ifdef LORA_433MHZ
    App_SX1278_Init();
#endif //LORA_433MHZ

#ifdef RGB_LCD
    halLCD_Init();
#endif // RGB_LCD

    for (uint8_t i = 0; i < 50; i++) // delay of 10ms
    {
        _delay_us(200);
    }

    //air pressure and temperature
    bmp180_init();

#ifdef NEO_Pixel
	gNeoPixel.begin();
#endif // NEO_Pixel

	LSM330DLC_lsm303dlhc_Init();

    //extAddress
    ds2411_read( (unsigned char *)(&appMsg.extAddr) );

    NWK_SetAddr(APP_ADDR);
    NWK_SetPanId(APP_PANID);
    PHY_SetChannel(APP_CHANNEL);
#ifdef PHY_AT86RF212
    PHY_SetBand(APP_BAND);
    PHY_SetModulation(APP_MODULATION);
#endif
    PHY_SetRxState(true);

#ifdef NWK_ENABLE_SECURITY
    NWK_SetSecurityKey((uint8_t *)APP_SECURITY_KEY);
#endif

    NWK_OpenEndpoint(APP_ENDPOINT, appDataInd);

    appDataSendingTimer.interval = APP_SENDING_INTERVAL;
    appDataSendingTimer.mode = SYS_TIMER_INTERVAL_MODE;
    appDataSendingTimer.handler = appDataSendingTimerHandler;

#if defined(APP_ROUTER) || defined(APP_ENDDEVICE)
    appNetworkStatus = false;
    appNetworkStatusTimer.interval = 500;
    appNetworkStatusTimer.mode = SYS_TIMER_PERIODIC_MODE;
    appNetworkStatusTimer.handler = appNetworkStatusTimerHandler;
    SYS_TimerStart(&appNetworkStatusTimer);

    appCommandWaitTimer.interval = NWK_ACK_WAIT_TIME;
    appCommandWaitTimer.mode = SYS_TIMER_INTERVAL_MODE;
    appCommandWaitTimer.handler = appCommandWaitTimerHandler;
#else
    // HAL_LedOn(APP_LED_NETWORK);
#endif

#ifdef PHY_ENABLE_RANDOM_NUMBER_GENERATOR
    srand(PHY_RandomReq());
#endif

    APP_CommandsInit();

    appState = APP_STATE_SENSING;
}

/*************************************************************************//**
*****************************************************************************/
static void APP_TaskHandler(void)
{
    switch (appState)
    {
        case APP_STATE_INITIAL:
            {
                appInit();
            } break;

        case APP_STATE_SENSING:

#if defined(FIX_ON_OFF_ARDIO)
    HAL_GPIO_ARD_DIO_out();
    HAL_GPIO_ARD_DIO_set();     //ARD DIO En
#endif // defined(FIX_ON_OFF_ARDIO)

#ifdef _COZIR_CO2_SENSOR_
        _delay_ms(1000);
#endif // _COZIR_CO2_SENSOR_

#ifdef _RS485_SENSOR_
            {
                appSensingData();
            } break;
#else
            {
                appState = APP_STATE_SEND;
            } break;
#endif //_RS485_SENSOR_

        case APP_STATE_SEND:
            {
                appSendData();
            } break;

        case APP_STATE_SENDING_DONE:
            {
#if defined(APP_ENDDEVICE)
                appState = APP_STATE_LORA_SEND; // Change for LoRa // APP_STATE_PREPARE_TO_SLEEP;
#else
                SYS_TimerStart(&appDataSendingTimer);
                appState = APP_STATE_WAIT_SEND_TIMER;
#endif
            } break;

        case APP_STATE_LORA_SEND:
            {
#ifdef LORA_433MHZ
                App_SX1278_DumpReg();
                appUartSendMessage(SX1278_Reg, 128);

                App_SX1278_Test();

                appLoRaSendTimer.interval = 3000;
                appLoRaSendTimer.mode = SYS_TIMER_INTERVAL_MODE;
                appLoRaSendTimer.handler = appLoRaSendTimerHandler;
                SYS_TimerStart(&appLoRaSendTimer);

                appState = APP_STATE_WAIT_LORA_SEND_TIMER;
#else
                appState = APP_STATE_PREPARE_TO_SLEEP;
#endif // LORA_433MHZ
            } break;

        case APP_STATE_PREPARE_TO_SLEEP:
            {
#ifdef LORA_433MHZ
                App_SX1278_DeInit();

                App_SX1278_DumpReg();
                appUartSendMessage(SX1278_Reg, 128);
#endif // LORA_433MHZ
                if (!NWK_Busy())
                {
                    NWK_SleepReq();
                    appState = APP_STATE_SLEEP;
                }
            } break;

        case APP_STATE_SLEEP:
            {
                HAL_LedOff(0);
                HAL_Sleep(APP_SENDING_INTERVAL);
                appState = APP_STATE_WAKEUP;
            } break;

        case APP_STATE_WAKEUP:
            {
                NWK_WakeupReq();

                HAL_LedInit();
                HAL_LedOn(0);

                appState = APP_STATE_SENSING;
            } break;

        default:
            break;
    }
}

/*************************************************************************//**
*****************************************************************************/
int main(void)
{
#ifndef WIN32  //disable JTAG
  MCUCR = (1<<JTD);
  MCUCR = (1<<JTD);  
#endif
    
    SYS_Init();
    HAL_UartInit(38400);
#ifdef HAL_Uart0_Actived
    #ifdef _YW51_PM25_SENSOR_
        HAL_Uart0Init(2400);
    #else
        HAL_Uart0Init(9600);
    #endif // _YW51_PM25_SENSOR_
#endif //HAL_Uart0_Actived
    HAL_LedInit();

#ifdef _SENSOR_DHT22_
    dht.begin();
#endif // _SENSOR_DHT22_

    while (1)
    {
        SYS_TaskHandler();
        HAL_UartTaskHandler();
#ifdef HAL_Uart0_Actived
        HAL_Uart0TaskHandler();
#endif //HAL_Uart0_Actived
#ifdef LORA_433MHZ
        App_SX1278_TaskHandle();
#endif //LORA_433MHZ
        APP_TaskHandler();

        // HAL_LedToggle(0);

        // volatile unsigned long i = 0;
        // for (i = 0; i < 0xFFFFF; i++)
        // {
        //     __asm__ __volatile__ ("nop");
        // }

    }
}

#ifdef __cplusplus
}
#endif
