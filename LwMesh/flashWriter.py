import os
import atexit
from time import sleep
import subprocess
import platform
import serial

if platform.system() == "Windows":
    MY_PORT = "COM52"
else:
    MY_PORT = "/dev/tty.usbserial-FTGA2JFX"

SREC_FILE_PATH = "./source/apps/WSNDemo/make/Debug/WSNDemo.srec"

MY_SERIAL = serial.Serial(MY_PORT, 38400, timeout=0)


def subprocess_cmd(command):
    process = subprocess.Popen(
        command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    proc_stdout = process.communicate()
    return proc_stdout


def writer():

    # read srec ...
    content = []

    try:
        f = open(SREC_FILE_PATH)
        content = f.readlines()
        f.close()
    except:
        print "Read Error (open failure): " + SREC_FILE_PATH
        return False

    if len(content) <= 0:
        print "Read Error (srec size=0): " + SREC_FILE_PATH
        return False

    print "\n\nFlash writer starts ......\n"

    # handshake ...
    count = 0
    handshake_req = bytearray([0xB2, 0xA5, 0x65, 0x4B])
    handshake_conf = "69d3d226"  # the manual is WRONG!

    print "Please click the RESET button on your board"

    while True:

        MY_SERIAL.write(handshake_req)

        hex_data = MY_SERIAL.read(50)
        if len(hex_data) > 0 and \
                hex_data.encode("hex").find(handshake_conf) != -1:
            break

        sleep(0.02)

        count += 1
        if count > 200:
            print "Please make sure you have clicked the RESET button !"
            count = 0

    # print "\nGot a handshake confirm"

    # start to upload ...
    print "Writing to the flash memory"

    count = 0
    a_record = []
    c_ary = []
    for line in content:

        cc = ""
        index = 0
        a_record = []
        c_ary = []
        for c in line:
            # for the first two marker only
            if index == 0 or index == 1:
                MY_SERIAL.write(c)
                index += 1
                continue

            if c == "\r" or c == "\n":
                break

            if cc == "":
                cc = c
            else:
                cc += c
                c_ary.append(cc)
                a_record.append(int(cc, 16))
                cc = ""

        count += 1

        MY_SERIAL.write(bytearray(a_record))

        for num in range(0, 1000):
            sleep(0.04)
            re = MY_SERIAL.read(20).encode("hex")
            # print "read try", num
            # print re

            if re.find("4d5a9ab4") >= 0:
                if count % 10 == 0:
                    print "Wrote " + str(count) + " lines"
                break
            else:
                print "Writing error on line: " + str(count)
                return False

    print "Wrote " + str(count) + " lines"
    print "\n...... Flash writer finished the writing\n"

    return True


def exit_handler():
    MY_SERIAL.close()
    print 'Finished clean up'


atexit.register(exit_handler)


def main():
    print "To kill: ctrl+c or sudo kill -9 ", os.getpid(), "\n"

    try:
        writer()
    except KeyboardInterrupt:
        exit_handler()


if __name__ == "__main__":
    main()
