import serial
from threading import Thread
import threading
from time import sleep
import serverLwMessage
import platform

try:
    # isRasp = True
    import RPi.GPIO as GPIO
    MY_PORT = "/dev/ttyAMA0"

except ImportError:
    # isRasp = False
    if platform.system() == "Windows":
        MY_PORT = "COM16"
    else:
        MY_PORT = "/dev/tty.usbserial-FTGA2JFX"

MY_SERIAL = serial.Serial(MY_PORT, 38400, timeout=0)


Is_Continue_Serail_Read = True
Serial_Raw_CH_Slot_List = []

MAX_FRAME_SIZE = 200

APP_CMD_UART_STATE_IDLE = 1
APP_CMD_UART_STATE_SYNC = 2
APP_CMD_UART_STATE_DATA = 3
APP_CMD_UART_STATE_MARK = 4
APP_CMD_UART_STATE_CSUM = 5

Frame_State = APP_CMD_UART_STATE_IDLE
Frame_Sum = 0
Last_Message = ""


def unpack_frame():

    global Serial_Raw_CH_Slot_List
    global Last_Message
    global Frame_State
    global Frame_Sum

    serial_raw_chars = ""

    size = len(Serial_Raw_CH_Slot_List)
    while size > 0:
        serial_raw_chars += Serial_Raw_CH_Slot_List.pop(0)
        size -= 1

    for ch in serial_raw_chars:

        ch_ord = ord(ch)

        if Frame_State is APP_CMD_UART_STATE_IDLE:

            if ch_ord is 0x10:
                Last_Message = ""
                Frame_Sum = ch_ord
                Frame_State = APP_CMD_UART_STATE_SYNC

        elif Frame_State is APP_CMD_UART_STATE_SYNC:

            Frame_Sum += ch_ord
            if ch_ord is 0x02:
                Frame_State = APP_CMD_UART_STATE_DATA
            else:
                Frame_State = APP_CMD_UART_STATE_IDLE

        elif Frame_State is APP_CMD_UART_STATE_DATA:

            Frame_Sum += ch_ord
            if ch_ord is 0x10:
                Frame_State = APP_CMD_UART_STATE_MARK
            else:
                Last_Message += ch

            if len(Last_Message) > MAX_FRAME_SIZE:
                Frame_State = APP_CMD_UART_STATE_IDLE

        elif Frame_State is APP_CMD_UART_STATE_MARK:

            Frame_Sum += ch_ord
            if ch_ord is 0x10:
                Last_Message += ch
                if len(Last_Message) > MAX_FRAME_SIZE:
                    Frame_State = APP_CMD_UART_STATE_IDLE
                else:
                    Frame_State = APP_CMD_UART_STATE_DATA
            elif ch_ord is 0x03:
                Frame_State = APP_CMD_UART_STATE_CSUM
            else:
                Frame_State = APP_CMD_UART_STATE_IDLE

        elif Frame_State is APP_CMD_UART_STATE_CSUM:

            if ch_ord is (Frame_Sum % 256):
                serverLwMessage.decode(Last_Message)

            Frame_State = APP_CMD_UART_STATE_IDLE

    if Is_Continue_Serail_Read:
        threading.Timer(1, unpack_frame).start()


def write(byte):
    MY_SERIAL.write(byte)


def read(len=1):
    return MY_SERIAL.read(len)


def read_once():
    global Serial_Raw_CH_Slot_List
    data = MY_SERIAL.read(500)
    if len(data) > 0:
        Serial_Raw_CH_Slot_List.append(data)


def read_loop():
    global Serial_Raw_CH_Slot_List
    global Is_Continue_Serail_Read
    while Is_Continue_Serail_Read:
        data = MY_SERIAL.read(50)
        if len(data) > 0:
            Serial_Raw_CH_Slot_List.append(data)
        sleep(0.1)


def start_loop():

    print "SP > ", "Start Serial Port"
    global Is_Continue_Serail_Read
    Is_Continue_Serail_Read = True

    # out
    unpack_frame()

    # in
    st = Thread(target=read_loop)
    st.daemon = True
    st.start()


def stop_loop():
    global Is_Continue_Serail_Read
    Is_Continue_Serail_Read = False


def shut_down():
    stop_loop()
    MY_SERIAL.close()
