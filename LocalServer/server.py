import os
import atexit
from time import sleep
import serverLwFrame


def exit_handler():
    serverLwFrame.shut_down()
    print 'Finished clean up'


atexit.register(exit_handler)


def main():
    print "To kill: ctrl+c or sudo kill -9 ", os.getpid()
    serverLwFrame.start_loop()
    try:
        while True:
            sleep(1)
    except KeyboardInterrupt:
        exit_handler()


if __name__ == "__main__":
    main()
