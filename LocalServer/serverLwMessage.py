import binascii
import datetime
import time

int8_t = 1
uint8_t = 1
int16_t = 2
uint16_t = 2
int32_t = 4
uint32_t = 4
uint64_t = 8
unknown_t = 20


def do_nothing(val):
    return None


def uint8_to_dec(val):
    return ord(val)


def int8_to_dec(val):
    unsigned = uint8_to_dec(val)
    signed = unsigned - 256 if unsigned > 127 else unsigned
    return signed


def uint64_to_hex_str(val):
    return "0x" + str(binascii.b2a_hex(val)).upper()


def any_r_to_hex_str(val):
    val = val[::-1]  # revered
    return "0x" + binascii.b2a_hex(val)


def convert_battery(val):
    num = ord(val[1]) * 256 + ord(val[0])
    # print num
    return(num * 1600 / 1024 * 11 + 360)


def convert_temperature(val):
    num = ord(val[1]) * 256 + ord(val[0])
    # print num
    num *= 0.5
    return num


def convert_light(val):
    num = ord(val[1]) * 256 + ord(val[0])
    # print num
    num = (num * 1600 / 1024 / 2 / 2)
    return num


def convert_int16(val):
    num = ord(val[1]) * 256 + ord(val[0])
    # print num
    # num = (num * 1600 / 1024 / 2 / 2)
    if num > 32768:
        num = num - 65536
    return num


def convert_caption(val):
    return val


def decode(msg):
    ptime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    pack = [
        {"field": "command_id", "length": uint8_t, "method": uint8_to_dec, "result": None},
        {"field": "node_type", "length": uint8_t, "method": uint8_to_dec, "result": None},
        {"field": "ext_addr", "length": uint64_t, "method": uint64_to_hex_str, "result": None},

        {"field": "short_addr", "length": uint16_t, "method": any_r_to_hex_str, "result": None},
        {"field": "soft_version", "length": uint32_t, "method": any_r_to_hex_str, "result": None},
        {"field": "channel_mask", "length": uint32_t, "method": any_r_to_hex_str, "result": None},
        {"field": "pan_id", "length": uint16_t, "method": any_r_to_hex_str, "result": None},
        {"field": "working_channel", "length": uint8_t, "method": uint8_to_dec, "result": None},
        {"field": "parent_short_addr", "length": uint16_t, "method": any_r_to_hex_str, "result": None},

        {"field": "lqi", "length": uint8_t, "method": uint8_to_dec, "result": None},
        {"field": "rssi", "length": int8_t, "method": int8_to_dec, "result": None},

        {"field": "sensors_type", "length": uint8_t, "method": uint8_to_dec, "result": None},
        {"field": "sensor_size", "length": uint8_t, "method": uint8_to_dec, "result": None},

        {"field": "battery", "length": int32_t, "method": convert_battery, "result": None},
        {"field": "temperature", "length": int32_t, "method": convert_temperature, "result": None},
        {"field": "light", "length": int32_t, "method": convert_light, "result": None},

        {"field": "sensors_bmp180_temperature", "length": int16_t, "method": do_nothing, "result": None},
        {"field": "sensors_bmp180_pressure", "length": uint32_t, "method": do_nothing, "result": None},

        {"field": "sensors_acc_X", "length": int16_t, "method": do_nothing, "result": None},
        {"field": "sensors_acc_Y", "length": int16_t, "method": do_nothing, "result": None},
        {"field": "sensors_acc_Z", "length": int16_t, "method": do_nothing, "result": None},

        {"field": "sensors_magnetic_X", "length": int16_t, "method": do_nothing, "result": None},
        {"field": "sensors_magnetic_Y", "length": int16_t, "method": do_nothing, "result": None},
        {"field": "sensors_magnetic_Z", "length": int16_t, "method": do_nothing, "result": None},

        {"field": "sensors_acc2_X", "length": int16_t, "method": do_nothing, "result": None},
        {"field": "sensors_acc2_Y", "length": int16_t, "method": do_nothing, "result": None},
        {"field": "sensors_acc2_Z", "length": int16_t, "method": do_nothing, "result": None},

        {"field": "sensors_gyroscope_X", "length": int16_t, "method": do_nothing, "result": None},
        {"field": "sensors_gyroscope_Y", "length": int16_t, "method": do_nothing, "result": None},

        {"field": "usu_frame_seq", "length": int16_t, "method": convert_int16, "result": None},
        {"field": "no2_we", "length": int16_t, "method": convert_int16, "result": None},
        {"field": "no2_ae", "length": int16_t, "method": convert_int16, "result": None},
        {"field": "o3_we", "length": int16_t, "method": convert_int16, "result": None},
        {"field": "o3_ae", "length": int16_t, "method": convert_int16, "result": None},

        {"field": "sensors_ozone4", "length": int16_t, "method": do_nothing, "result": None},
        {"field": "sensors_ozone5", "length": int16_t, "method": do_nothing, "result": None},

        {"field": "air_temperature", "length": int16_t, "method": convert_int16, "result": None},
        {"field": "air_humidity", "length": int16_t, "method": convert_int16, "result": None},

        {"field": "caption_type", "length": uint8_t, "method": uint8_to_dec, "result": None},
        {"field": "caption_size", "length": uint8_t, "method": uint8_to_dec, "result": None},
        {"field": "caption", "length": unknown_t, "method": convert_caption, "result": None}
    ]

    index = 0
    caption_size = 0

    for item in pack:
        length = item["length"]
        if caption_size != 0 and item["field"] == "caption":
            length = caption_size

        item["result"] = item["method"](msg[index: index + length])
        index += length

        if item["field"] == "caption_size":
            caption_size = item["result"]

    # the output to command-line interface
    cli_msg = ""
    # the output to log file
    log_msg = ptime
    log_msg_header = "Time"

    for item in pack:

        if item["result"] is None:
            continue

        cli_msg += (item["field"] + ": " + str(item["result"]) + "\n")

        log_msg_header += ("; " + item["field"])
        log_msg += ("; " + str(item["result"]))

    # just print to cli
    print "Time:", ptime
    print cli_msg

    log_msg += "\n"
    log_msg_header += "\n"
    f = open('log.csv', 'a')
    f.seek(0, 2)
    if f.tell() < 50:
        f.write(log_msg_header)
    f.write(log_msg)
    f.close()
